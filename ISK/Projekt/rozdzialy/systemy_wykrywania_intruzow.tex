\section{Systemy wykrywania intruzów (włamań)}
\subsection{Systemy IDS}

Zadaniem systemu wykrywania intruzów \ang{IDS, Intrusion Detection System} jest identyfikacja zagrożenia w sieci komputerowej. Podstawą wykrywania włamań jest monitorowanie ruchu w sieci. Systemy wykrywania włamań działają w oparciu o informacje odnoszące się do aktywności chronionego systemu – współczesne systemy IDS analizują w czasie rzeczywistym aktywność w sieci.

Włamanie do systemu najczęściej przebiega w dwóch etapach:

\begin{itemize}
\item \bold{Etap pierwszy} – próba penetracji systemu będącego celem ataku. Intruz usiłuje znaleźć lukę w systemie (na przykład próbuje skanować porty), umożliwiającą wtargnięcie do systemu poprzez ominięcie systemów zabezpieczających.
\item \bold{Etap drugi} – wtargnięcie do systemu. Jednocześnie odbywa się próba zamaskowania obecności intruza poprzez odpowiednie zmiany w logach systemowych. Włamywacz podejmuje również próby modyfikacji narzędzi systemowych tak, by uniemożliwić swoje wykrycie.
\end{itemize}

Systemy IDS analizują procesy zachodzące w newralgicznych obszarach sieci objętej ochroną. Umożliwiają więc wykrycie niepożądanych zajść podczas próby włamania oraz po udanym włamaniu – jest to bardzo ważne ze względów bezpieczeństwa, ponieważ IDS działa dwufazowo – nawet jeżeli intruz zdoła włamać się do systemu, nadal może zostać wykryty i unieszkodliwiony, mimo usilnego zacierania śladów swojej działalności.

Systemy IDS korzystają z czterech podstawowych metod, dzięki którym możliwe jest zidentyfikowanie intruza wewnątrz chronionej sieci:

\begin{itemize}
\item \bold{Dopasowywanie wzorców} – jest to najprostsza metoda detekcji intruza; pojedynczy pakiety porównywany jest z listą reguł. Jeśli któryś z warunków jest spełniony, to jest uruchamiany alarm.
\item \bold{Kontekstowe dopasowywanie wzorców} – w kontekstowym dopasowywaniu pakietu, system bierze pod uwagę kontekst każdego pakietu. Śledzi połączenia, dokonuje łączenia fragmentowanych pakietów.
\item \bold{Analiza heurystyczna} – wykorzystuje algorytmy do identyfikacji niepożądanego działania. Algorytmy te są zwykle statystyczną oceną normalnego ruchu sieciowego. Przykładowo, algorytm stwierdzający skanowanie portów wykazuje, że takie wydarzenie miało miejsce, jeżeli z jednego adresu w krótkim czasie nastąpi próba połączeń z wieloma portami.
\item \bold{Analiza anomalii} – sygnatury anomalii starają się wykryć ruch sieciowy, który odbiega od normy. Największym problemem jest określenie stanu uważanego za normalny.
\end{itemize}

Mimo ciągłego rozwoju systemów IDS, napotykają one na liczne przeszkody, które zniekształcają prawidłowe działanie oprogramowania:

\begin{itemize}
\item \bold{Mnogość aplikacji} – w przypadku ataku na konkretną aplikację, polegającym na podawaniu jej nietypowych danych, system musi "rozumieć" protokół, którego dana aplikacja używa. Protokołów sieciowych jest bardzo wiele i system IDS na ogół nie zna ich wszystkich, a tylko pewien ich podzbiór. Jest to wykorzystywane przy próbach ataku na sieć chronioną przez IDS.
\item \bold{Defragmentacja pakietów} – wykrycie ataku rozłożonego na kilka pakietów wymaga monitorowania przebiegu sesji. Takie działanie pochłania jednak część zasobów komputerowych: pamięć i czas.
\item \bold{Fałszywe alarmy}.
\item \bold{Ograniczenia zasobów} – zajęcie wszystkich zasobów sensora jest wykorzystywane do ataków na sieci chronione przez IDS.
\end{itemize}

Istnieją cztery główne rodzaje ataków, które systemy klasy IDS są w stanie rozpoznać:

\begin{itemize}
\item Nieautoryzowany dostęp do zasobów – najbardziej liczna grupa ataków zawierająca w sobie między innymi łamanie haseł dostępowych, używanie koni trojańskich oraz podszywanie się.
\item Nieuprawniona modyfikacja zasobów – to nieuprawnione modyfikacje, kasowanie danych oraz generowanie nieuprawnionych transmisji danych.
\item Blokowanie usług – przede wszystkim ataki typu DoS/DDoS.
\item Ataki zorientowane na aplikacje – ataki wykorzystujące błędy oraz luki zawarte w aplikacjach.
\end{itemize}

\subsection{ Rodzaje systemów IDS}

Działanie pierwszych systemów do wykrywania włamań polegało przede wszystkim na szczegółowej analizie wystąpienia niebezpiecznego zdarzenia. Współczesne aplikacje IDS wykonują dodatkowo monitorowanie sieci oraz wykrywanie i reagowanie w czasie rzeczywistym na nieautoryzowane działania w sieci. 

Wyróżnia się trzy główne rodzaje systemów IDS:

\begin{itemize}
\item NIDS \ang{Network Intrusion Detection System} – rozwiązania sprzętowe lub programowe śledzące sieć.
\item HIDS \ang{Host Intrusion Detection System} – aplikacje instalowane na chronionych serwerach usług sieciowych.
\item NNIDS \ang{Network Node Intrusion Detection System} – rozwiązania hybrydowe.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{./rys/nids.eps}
\caption{Schemat systemu wykrywania włamań typu NIDS}
\label{fig:nids}
\end{figure}

Na \rys{fig:nids} jest pokazany schemat sieciowego systemu wykrywania intruzów (NIDS). Takie rozwiązanie umożliwia skuteczne monitorowanie wydzielonego segmentu sieci. System NIDS może podsłuchiwać wszelką komunikację prowadzoną w tej sieci. To rozwiązanie jest nastawione na ochronę publicznie dostępnych serwerów zlokalizowanych w podsieciach stref zdemilitaryzowanych.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{./rys/hids.eps}
\caption{Schemat systemu wykrywania włamań typu HIDS}
\label{fig:hids}
\end{figure}

Schemat hostowego systemu wykrywania intruzów (HIDS) \rys{fig:hids}. Podstawowa różnica między systemami HIDS a NIDS polega na tym, że w pierwszym przypadku chroniony jest tylko komputer, na którym system rezyduje. Ponadto system HIDS można uruchamiać na zaporach ogniowych, zabezpieczając je w ten sposób.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{./rys/nnids.eps}
\caption{Schemat systemu wykrywania włamań typu NNIDS}
\label{fig:nnids}
\end{figure}

Hybrydowy system wykrywania intruzów (NNIDS) \rys{fig:nnids}, składający się z czterech sensorów i centralnego systemu zarządzającego. Standardowo systemy NNIDS funkcjonują w ramach architektury przeznaczonej do obsługi zarządzania i badania sieci. Sensory wykrywania włamań systemów NIDS są zlokalizowane zdalnie w odpowiednich miejscach i składają raporty do centralnego systemu zarządzania NIDS. 

Dzienniki ataków są co jakiś czas dostarczane do systemu zarządzającego i mogą być tam przechowywane w centralnej bazie danych. Z kolei nowe sygnatury ataków mogą być ładowane do systemów-sensorów. Zgodnie z przedstawionym schematem, sensory NIDS1 i NIDS2 operują w cichym trybie odbierania i chronią serwery dostępu publicznego. Z kolei sensory NIDS3 i NIDS4 chronią systemy hostów znajdujących się wewnątrz sieci zaufanej.
