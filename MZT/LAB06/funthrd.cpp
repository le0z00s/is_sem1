//////////////////////////////////////////////////////////////////////////////////
//   funthrd.cpp
//   thread function implementations

#include "stdafx.h"
#include <iostream>
#include "timethrd.h"

using namespace std;

DWORD WINAPI ThreadFunc1(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);

	CThreadTime *thrd_timer = ptrDat->thrd_timer;
	thrd_timer->begin();

	ptrDat->ret = 0;

	//tDat[ip].ip = ip; // liczba podzialow na watek
	//tDat[ip].ndiv = ndiv; // liczba podzialow w ogole
	//tDat[ip].np = np; // liczba watkow
	//tDat[ip].a = a; // koniec przedzialow calkowania [0, a]
	//tDat[ip].b = b;
	//tDat[ip].c = c;
	//tDat[ip].dx = dx; // podzial
	//tDat[ip].p_fun = &fun; // funkcja podcalkowa
	//tDat[ip].thrd_timer = &thrd_timer[ip]; // timer


	int i,
		forThread = ptrDat->ndiv / ptrDat->np;
	double x,  //Wspolrzedna x poczatku odcinku dx 
		fk,  //wartosc funkcji podintegralnej na poczatku odcinku dx  
		fk1,  //wartosc funkcji podintegralnej na koncu odcinku dx  
		s;    //suma = suma + (f_b+f_e)*dx/2 - metoda trapezu

	s = 0.0;

	double startx = ptrDat->a * ((double)ptrDat->ip / (double)ptrDat->np);

	//cout << "Watek " << ptrDat->ip << " startuje od " << startx << endl;

	//for (int j = 0; j < 10; ++j){
		s = 0;
		x = startx;
		fk = ptrDat->p_fun(x, ptrDat->b, ptrDat->c);

		for (i = 1; i <= forThread; ++i){
			x = startx + i * ptrDat->dx;
			fk1 = ptrDat->p_fun(x, ptrDat->b, ptrDat->c);
			s += (fk + fk1) / 2 * ptrDat->dx;
			fk = fk1;
		}
	//}


	//cout << "Watek " << ptrDat->ip << " konczy na " << x << endl;

	ptrDat->s = s;
	thrd_timer->getelapstime();
	ptrDat->ret = 0;

	return 0;
}