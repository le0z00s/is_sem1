// Lab_dot_prod.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include "timethrd.h"

using namespace std;

#define NUMB_THREADS 10

int start(int N, int np, int ntimes, LPTHREAD_START_ROUTINE threadFunc)
{
	HANDLE hThread[NUMB_THREADS];
	DWORD ThreadID[NUMB_THREADS];
	THREAD_DATA tDat[NUMB_THREADS];
	double *X = NULL, *Y = NULL;
	double dotProd;

	int /*N, */ip,/* np,*/ loc_N;
	/*int ntimes;*/

#ifdef _DEBUG
	cout << "START dotProd: DEBUG VERSION\n";
#else
	cout << "START dotProd: RELEASE VERSION\n";
#endif
	cout << "Get N\n";
	cout << N << endl;
	//cin >> N;
	cout << "Get number of processors\n";
	cout << np << endl;
	//cin >> np;
	cout << "Get number of repetitions\n";
	cout << ntimes << endl;
	//cin >> ntimes;

	loc_N = N / np;
	N = loc_N*np;

	cout << "actual N = " << N << endl;
	cout << "np = " << np << endl;

	try
	{
		X = new double[N];
		Y = new double[N];
	}
	catch (bad_alloc aa)
	{
		cout << "memory allocation error\n";
		system("pause");
		exit(1);
	}

	//preparation of X, Y
	int i;
	for (i = 0; i < N; i++)
	{
		X[i] = (double)(i + 1);
		Y[i] = 1.0;
	}

	DWORD time_st = GetTickCount();

	for (ip = 0; ip < np; ip++)
	{
		//wypelnic dane dla potoku ip
		tDat[ip].loc_N = loc_N;
		tDat[ip].ntimes = ntimes;
		tDat[ip].X = &X[loc_N*ip];
		tDat[ip].Y = &Y[loc_N*ip];

		//stworzyc potok ip

		// HANDLE WINAPI CreateThread(
		//      _In_opt_  LPSECURITY_ATTRIBUTES  lpThreadAttributes,
		//      _In_      SIZE_T                 dwStackSize,
		//      _In_      LPTHREAD_START_ROUTINE lpStartAddress,
		//      _In_opt_  LPVOID                 lpParameter,
		//      _In_      DWORD                  dwCreationFlags,
		//      _Out_opt_ LPDWORD                lpThreadId
		// );

		// NULL - bez security descriptora
		// 0 - domy�lny rozmiar stosu
		// &ThreadFun - adres funkcji w�tku
		// (void*)&tDat[ip] - dane dla w�tku
		// 0 - posta� aktywna, CREATE_SUSPENDED - w�tek w postaci zawieszonej
		// &ThreadID[ip] - adres do umieszczenia w nim identyfikatora w�tku

		hThread[ip] = CreateThread(NULL, 0, threadFunc, (void*)&tDat[ip], 0, &ThreadID[ip]);
		if (hThread[ip] == NULL)
		{

		}
	}

	// zawiesic pierwotny potok dokad potoki liczace nie skonca prace
	// DWORD WINAPI WaitForMultipleObjects(
	//      _In_       DWORD  nCount,
	//      _In_ const HANDLE *lpHandles,
	//      _In_       BOOL   bWaitAll,
	//      _In_       DWORD  dwMilliseconds
	// );
	WaitForMultipleObjects(np, hThread, true, INFINITE);

	//Check
	bool IsOK = true;
	for (ip = 0; ip < np; ip++)
	{
		if (tDat[ip].ret != 0)
		{
			cout << " Thread # " << ip + 1 << " failed" << endl;
			IsOK = false;
		}
		CloseHandle(hThread[ip]);
	}

	if (!IsOK)
	{
		system("pause");
		exit(1);
	}

	//reduction: sumowanie 
	dotProd = 0.0;

	for (ip = 0; ip < np; ip++)
	{
		dotProd += tDat[ip].res;
	}

	cout << "WTime = " << GetTickCount() - time_st << " ms\n";

	//check
	double sum = 0.0;
	for (i = 0; i < N; i++)
	{
		sum += X[i] * Y[i];
	}

	if ((fabs(sum - dotProd)) > 1.0e-9)
	{
		cout << "error\n";
		cout << "!!!!!!!!!!!\n";
	}

	cout << "END MV\n";
	system("pause");

	delete[] X;
	delete[] Y;

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "******** ThreadFunc1 ********" << endl;
	//start(10000, 1, 1000000, &ThreadFunc1);
	//start(10000, 2, 1000000, &ThreadFunc1);
	//start(10000, 3, 1000000, &ThreadFunc1);
	start(10000, 4, 1000000, &ThreadFunc1);
	cout << "******** ThreadFunc2 ********" << endl;
	//start(10000, 1, 10, &ThreadFunc2);
	//start(10000, 2, 10, &ThreadFunc2);
	//start(10000, 3, 10, &ThreadFunc2);
	start(10000, 4, 10, &ThreadFunc2);
}