//////////////////////////////////////////////////////////////////////////////////
//   funthrd.cpp
//   thread function implementations

#include "stdafx.h"
#include <iostream>
#include "timethrd.h"
#include<cmath>
using namespace std;

DWORD WINAPI ThreadFunc1(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	double dot;
	int i, it;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		dot = 0;
		for (i = 0; i < ptrDat->loc_N; ++i)
		{
			dot += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
		}
	}

	ptrDat->res = dot;
	ptrDat->ret = 0;
	return 0;
}

DWORD WINAPI ThreadFunc2(LPVOID lpParam)
{
	__declspec(align(16)) double res[2];
	__m128d x1, x2, x3, x4, y1, y2, y3, y4, sum1, sum2, sum3, sum4;
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	int i, it;
	double dot;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		sum1 = sum2 = sum3 = sum4 = _mm_setzero_pd();
		dot = 0;

		for (i = 0; i < ptrDat->loc_N; ++i)
		{
			/*if (i % 8 == 0){
				_mm_prefetch((const char *)(ptrDat->X + i + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptrDat->Y + i + 8), _MM_HINT_T0);
			}*/

			dot += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
			
			x1 = _mm_load_pd((double*)(ptrDat->X + i));
			/*
			x2 = _mm_load_pd((double*)(ptrDat->X + i + 2));
			x3 = _mm_load_pd((double*)(ptrDat->X + i + 4));
			x4 = _mm_load_pd((double*)(ptrDat->X + i + 6));
			/*
			y1 = _mm_load_pd(ptrDat->Y + i);
			y2 = _mm_load_pd(ptrDat->Y + i + 2);
			y3 = _mm_load_pd(ptrDat->Y + i + 4);
			y4 = _mm_load_pd(ptrDat->Y + i + 6);

			x1 = _mm_mul_pd(x1, y1);
			x2 = _mm_mul_pd(x2, y2);
			x3 = _mm_mul_pd(x3, y3);
			x4 = _mm_mul_pd(x4, y4);

			sum1 = _mm_add_pd(sum1, x1);
			sum2 = _mm_add_pd(sum2, x2);
			sum3 = _mm_add_pd(sum3, x3);
			sum4 = _mm_add_pd(sum4, x4);
			*/
		}

		sum1 = _mm_add_pd(sum1, sum2);
		sum3 = _mm_add_pd(sum3, sum4);
		sum1 = _mm_add_pd(sum1, sum3);
		_mm_store_pd(res, sum1);
		ptrDat->res = res[0] + res[1];
	}

	ptrDat->res = dot;
	ptrDat->ret = 0;
	return 0;
}

DWORD WINAPI ThreadFunc3(LPVOID lpParam) // SSE2
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	int i, it;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		ptrDat->res = 0;
		for (i = 0; i < ptrDat->loc_N; ++i)
		{
			ptrDat->res += 0;
		}
	}

	ptrDat->ret = 0;
	return 0;
}