// Lab_dot_prod.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include "timethrd.h"

using namespace std;

#define NUMB_THREADS 10
typedef DWORD(*threadFunctionPointer)(LPVOID);

void start(int N, int np, int ntimes, LPTHREAD_START_ROUTINE threadFunc)
{
	HANDLE hThread[NUMB_THREADS];
	DWORD ThreadID[NUMB_THREADS];
	THREAD_DATA tDat[NUMB_THREADS];
	double *X = NULL, *Y = NULL;
	double dotProd;

	int ip, loc_N;

	cout << "N = " << N << endl;
	cout << "Number of processors = " << np << endl;
	cout << "Number of repetitions = " << ntimes << endl;

	loc_N = N / np;
	N = loc_N * np;

	cout << "Actual N = " << N << endl;

	try
	{
		//X = new double[N];
		//Y = new double[N];
		X = (double *)_aligned_malloc(N * sizeof(double), 16);
		Y = (double *)_aligned_malloc(N * sizeof(double), 16);

		if (X == NULL || Y == NULL) {
			throw new bad_alloc();
		}
	}
	catch (bad_alloc aa)
	{
		cout << "Memory allocation error" << endl;;
		system("pause");
		exit(1);
	}

	//preparation of X, Y
	int i;
	for (i = 0; i < N; i++)
	{
		X[i] = (double)(i + 1);
		Y[i] = 1.0;
	}

	DWORD time_st = GetTickCount();

	for (ip = 0; ip < np; ip++)
	{
		//wypelnic dane dla potoku ip
		tDat[ip].ntimes = ntimes;

		if (loc_N % 2 == 1 && (ip != np - 1 || ip % 2 != 0)) {
			if (ip % 2 == 0) {
				tDat[ip].loc_N = loc_N + 1;
				tDat[ip].X = &X[loc_N*ip];
				tDat[ip].Y = &Y[loc_N*ip];
				//cout << "ip " << ip << " loc_N " << tDat[ip].loc_N << " starts from " << (loc_N*ip) << endl;
			}
			else {
				tDat[ip].loc_N = loc_N - 1;
				tDat[ip].X = &X[loc_N*ip] + 1;
				tDat[ip].Y = &Y[loc_N*ip] + 1;
				//cout << "ip " << ip << " loc_N " << tDat[ip].loc_N << " starts from " << (loc_N*ip + 1) << endl;
			}
		}
		else {
			tDat[ip].loc_N = loc_N;
			tDat[ip].X = &X[loc_N*ip];
			tDat[ip].Y = &Y[loc_N*ip];
			//cout << "ip " << ip << " loc_N " << tDat[ip].loc_N << " starts from " << (loc_N*ip) << endl;
		}

		//cout << "pn " << tDat[ip].loc_N << endl;

		//stworzyc potok ip

		// HANDLE WINAPI CreateThread(
		//      _In_opt_  LPSECURITY_ATTRIBUTES  lpThreadAttributes,
		//      _In_      SIZE_T                 dwStackSize,
		//      _In_      LPTHREAD_START_ROUTINE lpStartAddress,
		//      _In_opt_  LPVOID                 lpParameter,
		//      _In_      DWORD                  dwCreationFlags,
		//      _Out_opt_ LPDWORD                lpThreadId
		// );

		// NULL - bez security descriptora
		// 0 - domy�lny rozmiar stosu
		// &ThreadFun - adres funkcji w�tku
		// (void*)&tDat[ip] - dane dla w�tku
		// 0 - posta� aktywna, CREATE_SUSPENDED - w�tek w postaci zawieszonej
		// &ThreadID[ip] - adres do umieszczenia w nim identyfikatora w�tku

		hThread[ip] = CreateThread(NULL, 0, threadFunc, (void*)&tDat[ip], 0, &ThreadID[ip]);
		if (hThread[ip] == NULL)
		{

		}
	}

	// zawiesic pierwotny potok dokad potoki liczace nie skonca prace
	// DWORD WINAPI WaitForMultipleObjects(
	//      _In_       DWORD  nCount,
	//      _In_ const HANDLE *lpHandles,
	//      _In_       BOOL   bWaitAll,
	//      _In_       DWORD  dwMilliseconds
	// );
	WaitForMultipleObjects(np, hThread, true, INFINITE);

	//Check
	bool IsOK = true;
	for (ip = 0; ip < np; ip++)
	{
		if (tDat[ip].ret != 0)
		{
			cout << "\tThread # " << ip + 1 << " failed" << endl;
			IsOK = false;
		}
		CloseHandle(hThread[ip]);
	}

	if (!IsOK)
	{
		system("pause");
		exit(1);
	}

	//reduction: sumowanie
	dotProd = 0.0;

	for (ip = 0; ip < np; ip++)
	{
		dotProd += tDat[ip].res;
	}

	cout << "WTime = " << GetTickCount() - time_st << " ms" << endl;

	//check
	double sum = 0.0;
	for (i = 0; i < N; i++)
	{
		sum += X[i] * Y[i];
	}

	if ((fabs(sum - dotProd)) > 1.0e-9)
	{
		cout << "Error! Bad result! Sum: " << sum << " dotProd: " << dotProd << endl;
	}

	//delete[] X;
	//delete[] Y;
	_aligned_free(X);
	_aligned_free(Y);

	cout << "*****************************" << endl << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int N = 10000000;
	int rep = 1000;
	int i = 0;


#ifdef _DEBUG
	cout << "START dotProd: DEBUG VERSION\n";
#else
	cout << "START dotProd: RELEASE VERSION\n";
#endif
	cout << endl;

	cout << "******** NAIVE - LOCAL VARIABLE ********" << endl;
	for (i = 1; i <= 4; ++i) {
		start(N, i, rep, &Naive_LocalVariable);
	}
	cout << "******** SSE2 - LOCAL VARIABLE ********" << endl;
	for (i = 1; i <= 4; ++i) {
		start(N, i, rep, &SSE2_LocalVariable);
	}
	cout << "******** NAIVE - STRUCTURE ELEMENT ********" << endl;
	for (i = 1; i <= 4; ++i) {
		start(N, i, rep, &Naive_StructureElement);
	}
	cout << "******** SSE2 - STRUCTURE ELEMENT ********" << endl;
	for (i = 1; i <= 4; ++i) {
		start(N, i, rep, &SSE2_StructureElement);
	}

	cout << endl << "END MV" << endl;
	system("pause");
	return 0;
}