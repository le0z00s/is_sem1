//////////////////////////////////////////////////////////////////////////////////
//   funthrd.cpp
//   thread function implementations

#include "stdafx.h"
#include <iostream>
#include "timethrd.h"
#include<cmath>
using namespace std;

DWORD WINAPI Naive_LocalVariable(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	double dot;
	int i, it;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		dot = 0;
		for (i = 0; i < ptrDat->loc_N; ++i)
		{
			dot += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
		}
	}

	ptrDat->res = dot;
	ptrDat->ret = 0;
	return 0;
}

DWORD WINAPI Naive_StructureElement(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	int i, it;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		ptrDat->res = 0;
		for (i = 0; i < ptrDat->loc_N; ++i)
		{
			ptrDat->res += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
		}
	}

	ptrDat->ret = 0;
	return 0;
}

DWORD WINAPI SSE2_LocalVariable(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	int i, it;
	__m128d x1, x2, x3, x4, y1, y2, y3, y4, sum1, sum2, sum3, sum4;
	__declspec(align(16)) double res[2];
	double *ptr_x, *ptr_y;
	double rest;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		sum1 = _mm_setzero_pd();
		sum2 = _mm_setzero_pd();
		sum3 = _mm_setzero_pd();
		sum4 = _mm_setzero_pd();
		rest = 0;

		for (i = 0, ptr_x = ptrDat->X, ptr_y = ptrDat->Y; i + 7 < ptrDat->loc_N; i += 8, ptr_x += 8, ptr_y += 8)
		{
			if (i + 7 < ptrDat->loc_N) {
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
			}

			x1 = _mm_load_pd(ptr_x);
			x2 = _mm_load_pd(ptr_x + 2);
			x3 = _mm_load_pd(ptr_x + 4);
			x4 = _mm_load_pd(ptr_x + 6);

			y1 = _mm_load_pd(ptr_y);
			y2 = _mm_load_pd(ptr_y + 2);
			y3 = _mm_load_pd(ptr_y + 4);
			y4 = _mm_load_pd(ptr_y + 6);

			x1 = _mm_mul_pd(x1, y1);
			x2 = _mm_mul_pd(x2, y2);
			x3 = _mm_mul_pd(x3, y3);
			x4 = _mm_mul_pd(x4, y4);

			sum1 = _mm_add_pd(sum1, x1);
			sum2 = _mm_add_pd(sum2, x2);
			sum3 = _mm_add_pd(sum3, x3);
			sum4 = _mm_add_pd(sum4, x4);
		}

		for (; i < ptrDat->loc_N; ++i)
		{
			rest += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
		}
	}

	sum1 = _mm_add_pd(sum1, sum2);
	sum3 = _mm_add_pd(sum3, sum4);
	sum1 = _mm_add_pd(sum1, sum3);

	_mm_store_pd(res, sum1);

	ptrDat->res = res[0] + res[1] + rest;
	ptrDat->ret = 0;
	return 0;
}

DWORD WINAPI SSE2_StructureElement(LPVOID lpParam)
{
	THREAD_DATA *ptrDat = (THREAD_DATA *)(lpParam);
	int i, it;
	__m128d x1, x2, x3, x4, y1, y2, y3, y4;
	__declspec(align(16)) double res[8];
	double *ptr_x, *ptr_y;

	for (it = 0; it < ptrDat->ntimes; ++it)
	{
		ptrDat->res = 0;

		for (i = 0, ptr_x = ptrDat->X, ptr_y = ptrDat->Y; i + 7 < ptrDat->loc_N; i += 8, ptr_x += 8, ptr_y += 8)
		{
			if (i + 7 < ptrDat->loc_N) {
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
			}

			x1 = _mm_load_pd(ptr_x);
			y1 = _mm_load_pd(ptr_y);
			x1 = _mm_mul_pd(x1, y1);
			_mm_store_pd(&res[0], x1);

			x2 = _mm_load_pd(ptr_x + 2);
			y2 = _mm_load_pd(ptr_y + 2);
			x2 = _mm_mul_pd(x2, y2);
			_mm_store_pd(&res[2], x2);

			x3 = _mm_load_pd(ptr_x + 4);
			y3 = _mm_load_pd(ptr_y + 4);
			x3 = _mm_mul_pd(x3, y3);
			_mm_store_pd(&res[4], x3);

			x4 = _mm_load_pd(ptr_x + 6);
			y4 = _mm_load_pd(ptr_y + 6);
			x4 = _mm_mul_pd(x4, y4);
			_mm_store_pd(&res[6], x4);

			ptrDat->res += res[0] + res[1] + res[2] + res[3] + res[4] + res[5] + res[6] + res[7];
		}

		for (; i < ptrDat->loc_N; ++i)
		{
			ptrDat->res += (ptrDat->X + i)[0] * (ptrDat->Y + i)[0];
		}
	}

	ptrDat->ret = 0;
	return 0;
}