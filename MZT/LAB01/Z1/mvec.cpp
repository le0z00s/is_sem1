////////////////////////////////////////////////////////////////////////////////
//  mvec.cpp

////////////////////////////////////////////////////////////////////////////////
//  mvec.cpp

#include "stdafx.h"
#include<iostream>
#include <emmintrin.h>
#include "inc_head.h"

using namespace std;

void matvec_orgin(double* a, double* x, double* y, int n)
{
	int i, j;

	for(i=0;i<n;i++){
	  y[i]=0.0;
	  for(j=0;j<n;j++){
	    y[i]+=a[i+n*j]*x[j];
	  }
	}
}


void matvec_opt_1(double* a, double* x, double* y, int n)
/*=========================================================================
Usuniecie skokow w danych
===========================================================================*/
{
	int i, j, ij = 0;
	register double r;

	memset((void *)y, 0, n * sizeof(double));

	for (j = 0; j < n; j++) {
		r = x[j];
		for (i = 0; i < n; i++) {
			y[i] += a[ij] * r;
			++ij;
		}
	}
}

void matvec_opt_2(double* a, double* x, double* y, int n)
/*============================================================================
Rozwijanie petli
=============================================================================*/
{
	int i, j, ij = 0;
	//int rest = n % 4;
	register double r;

	memset((void *)y, 0, n * sizeof(double));

	for (j = 0; j < n; ++j) {
		r = x[j];
		for (i = 0; i < n /* n-rest */; i += 8) {
			y[i] += a[ij] * r;
			y[i + 1] += a[ij + 1] * r;
			y[i + 2] += a[ij + 2] * r;
			y[i + 3] += a[ij + 3] * r;
			y[i + 4] += a[ij + 4] * r;
			y[i + 5] += a[ij + 5] * r;
			y[i + 6] += a[ij + 6] * r;
			y[i + 7] += a[ij + 7] * r;

			ij += 8;
		}
		for (; i < n; ++i) {
			y[i] += a[++ij] * r;
		}
	}
}

void matvec_opt_3(double* a, double* x, double* y, int n, int lb)
{
	int i, j;
	
	memset((void *)y, 0, n*sizeof(double));

	__m128d ry0, ry1, ry2, ry3, ra0, ra1, ra2, ra3, rx0;
	double *ptr_a, *ptr_x, *ptr_y;
	const int mr = 8;
	if(mr != lb)
	{
		cout << "lb != mr\n";
		system("pause");
		exit(1);
	}

	ptr_a = a;

	for(i=0; i<n; i+=mr)
	{
		ry0 = _mm_setzero_pd();
		ry1 = _mm_setzero_pd();
		ry2 = _mm_setzero_pd();
		ry3 = _mm_setzero_pd();
		
		//ptr_a = &a[n*i];
		ptr_y = &y[i];
		ptr_x = x;

		for(j=0; j<n; j++)
		{
			//_mm_prefetch((const char *)(ptr_a+8), _MM_HINT_T0);
			_mm_prefetch((const char *)(ptr_a+8), _MM_HINT_NTA);
			if (j % mr == 0)
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
			rx0 = _mm_load1_pd(ptr_x);
			ra0 = _mm_load_pd(ptr_a);
			ra1 = _mm_load_pd(ptr_a+2);
			ra2 = _mm_load_pd(ptr_a+4);
			ra3 = _mm_load_pd(ptr_a+6);

			ptr_a += mr;
			ptr_x++;

			ra0 = _mm_mul_pd(ra0, rx0);
			ra1 = _mm_mul_pd(ra1, rx0);
			ra2 = _mm_mul_pd(ra2, rx0);
			ra3 = _mm_mul_pd(ra3, rx0);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);
		}

		_mm_store_pd(ptr_y, ry0);
		_mm_store_pd(ptr_y+2, ry1);
		_mm_store_pd(ptr_y+4, ry2);
		_mm_store_pd(ptr_y+6, ry3);
	}
}

void matvec_opt_4(double* a, double* x, double* y, int n, int lb)
{
	int i, j;
	
	memset((void *)y, 0, n*sizeof(double));

	__m128d ry0, ry1, ry2, ry3, ra0, ra1, ra2, ra3, rx0;
	double *ptr_a, *ptr_x, *ptr_y;
	const int mr = 4;
	const int nr = 8;
	if(nr != lb || mr != lb/2)
	{
		cout << "lb != mr || lb != nr\n";
		system("pause");
		exit(1);
	}

	ptr_a = a;

	for(i=0; i<n; i+=nr)
	{
		ry0 = _mm_setzero_pd();
		ry1 = _mm_setzero_pd();
		ry2 = _mm_setzero_pd();
		ry3 = _mm_setzero_pd();
				
		//ptr_a = &a[n*i];
		ptr_y = &y[i];
		ptr_x = x;

		for(j=0; j<n; j+=mr)
		{
			//_mm_prefetch((const char *)(ptr_a+nr*mr), _MM_HINT_T0);
			_mm_prefetch((const char *)(ptr_a+nr*mr), _MM_HINT_NTA);
			if(j%mr == 0)
				_mm_prefetch((const char *)(ptr_x+nr), _MM_HINT_T0);
			//--------------------------0
			rx0 = _mm_load1_pd(ptr_x);
			ra0 = _mm_load_pd(ptr_a);
			ra1 = _mm_load_pd(ptr_a+2);
			ra2 = _mm_load_pd(ptr_a+4);
			ra3 = _mm_load_pd(ptr_a+6);

			ra0 = _mm_mul_pd(ra0, rx0);
			ra1 = _mm_mul_pd(ra1, rx0);
			ra2 = _mm_mul_pd(ra2, rx0);
			ra3 = _mm_mul_pd(ra3, rx0);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);

			//--------------------------1
			rx0 = _mm_load1_pd(ptr_x+1);
			ra0 = _mm_load_pd(ptr_a+8);
			ra1 = _mm_load_pd(ptr_a+10);
			ra2 = _mm_load_pd(ptr_a+12);
			ra3 = _mm_load_pd(ptr_a+14);

			ra0 = _mm_mul_pd(ra0, rx0);
			ra1 = _mm_mul_pd(ra1, rx0);
			ra2 = _mm_mul_pd(ra2, rx0);
			ra3 = _mm_mul_pd(ra3, rx0);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);

			//--------------------------2
			rx0 = _mm_load1_pd(ptr_x+2);
			ra0 = _mm_load_pd(ptr_a+16);
			ra1 = _mm_load_pd(ptr_a+18);
			ra2 = _mm_load_pd(ptr_a+20);
			ra3 = _mm_load_pd(ptr_a+22);

			ra0 = _mm_mul_pd(ra0, rx0);
			ra1 = _mm_mul_pd(ra1, rx0);
			ra2 = _mm_mul_pd(ra2, rx0);
			ra3 = _mm_mul_pd(ra3, rx0);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);
			
			//--------------------------3
			rx0 = _mm_load1_pd(ptr_x+3);
			ra0 = _mm_load_pd(ptr_a+24);
			ra1 = _mm_load_pd(ptr_a+26);
			ra2 = _mm_load_pd(ptr_a+28);
			ra3 = _mm_load_pd(ptr_a+30);

			ra0 = _mm_mul_pd(ra0, rx0);
			ra1 = _mm_mul_pd(ra1, rx0);
			ra2 = _mm_mul_pd(ra2, rx0);
			ra3 = _mm_mul_pd(ra3, rx0);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);

			ptr_a += mr*nr;
			ptr_x += mr;
		}

		_mm_store_pd(ptr_y, ry0);
		_mm_store_pd(ptr_y+2, ry1);
		_mm_store_pd(ptr_y+4, ry2);
		_mm_store_pd(ptr_y+6, ry3);
	}
}