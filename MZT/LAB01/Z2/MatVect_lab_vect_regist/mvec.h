///////////////////////////////////////////////////////////////////////////
//   mvec.h

#ifndef MVEC_HEADER_INCLUDE__H
#define MVEC_HEADER_INCLUDE__H

void check      (double *y, double *z, int n);
void prepare    (double *a, double *x, int n);
void mult_naive (double *a, double *x, double *y, int n);
void matvec_XMM (double* a, double* x, double* y, int n, int lb);
void matvec_XMM2(double* a, double* x, double* y, int n, int lb);
/*void matvec_XMM3(double* a, double* x, double* y, int n, int lb);*/

#endif