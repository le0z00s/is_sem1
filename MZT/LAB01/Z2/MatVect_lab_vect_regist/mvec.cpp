////////////////////////////////////////////////////////////////////////////////////////////////////
//   mvec.cpp

#include "stdafx.h"
#include <iostream>
#include "mvec.h"
#include <emmintrin.h>

using namespace std;

void mult_naive(double *a, double *x, double *y, int n)
{
	int i, j, ij;
	double register reg;

	for (i = 0, ij = 0; i < n; ++i)
	{
		reg = 0;

		for (j = 0; j < n; ++j, ++ij)
		{
			reg += a[ij] * x[j];
		}

		y[i] = reg;
	}
}

void matvec_XMM(double* a, double* x, double* y, int n, int lb)
{
	int i, j;
	double *ptr_a = a, *ptr_x;
	const int sj = 2;
	__m128d ra, rx, ry;
	__declspec(align(16)) double res[2];

	if (lb%sj != 0)
	{
		cout << "lb % sj != 0\n";
		system("pause");
		exit(1);
	}

	for (i = 0; i < n; ++i)
	{
		ry = _mm_setzero_pd();

		for (j = 0, ptr_x = x; j < n; j += sj, ptr_a += sj, ptr_x += sj)
		{
			ra = _mm_load_pd(ptr_a);
			rx = _mm_load_pd(ptr_x);

			ra = _mm_mul_pd(ra, rx);
			ry = _mm_add_pd(ry, ra);
		}

		_mm_store_pd(res, ry);

		y[i] = res[0] + res[1];
	}
}

void matvec_XMM2(double* a, double* x, double* y, int n, int lb)
{
	int i, j;
	double *ptr_a0, *ptr_a1, *ptr_a2, *ptr_a3, *ptr_x;
	const int sj = 2, si = 4, s = si*sj;
	__m128d ra0, ra1, ra2, ra3, rx, ry0, ry1, ry2, ry3;
	__declspec(align(16)) double res[2];

	if (lb%s != 0)
	{
		cout << "lb % s != 0\n";
		system("pause");
		exit(1);
	}

	for (i = 0; i < n; i += si)
	{
		ptr_a0 = &a[i*n];
		ptr_a1 = &a[(i + 1)*n];
		ptr_a2 = &a[(i + 2)*n];
		ptr_a3 = &a[(i + 3)*n];

		ry0 = _mm_setzero_pd();
		ry1 = _mm_setzero_pd();
		ry2 = _mm_setzero_pd();
		ry3 = _mm_setzero_pd();

		for (j = 0, ptr_x = x; j < n; j += sj, ptr_a0 += sj, ptr_a1 += sj, ptr_a2 += sj, ptr_a3 += sj, ptr_x += sj)
		{
			if (j % 8 == 0) {
				_mm_prefetch((const char *)(ptr_a0 + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_a1 + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_a2 + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_a3 + 8), _MM_HINT_T0);
				_mm_prefetch((const char *)(ptr_x + 8), _MM_HINT_T0);
			}

			ra0 = _mm_load_pd(ptr_a0);
			ra1 = _mm_load_pd(ptr_a1);
			ra2 = _mm_load_pd(ptr_a2);
			ra3 = _mm_load_pd(ptr_a3);

			rx = _mm_load_pd(ptr_x);

			ra0 = _mm_mul_pd(ra0, rx);
			ra1 = _mm_mul_pd(ra1, rx);
			ra2 = _mm_mul_pd(ra2, rx);
			ra3 = _mm_mul_pd(ra3, rx);

			ry0 = _mm_add_pd(ry0, ra0);
			ry1 = _mm_add_pd(ry1, ra1);
			ry2 = _mm_add_pd(ry2, ra2);
			ry3 = _mm_add_pd(ry3, ra3);
		}

		_mm_store_pd(res, ry0);
		y[i] = res[0] + res[1];
		_mm_store_pd(res, ry1);
		y[i + 1] = res[0] + res[1];
		_mm_store_pd(res, ry2);
		y[i + 2] = res[0] + res[1];
		_mm_store_pd(res, ry3);
		y[i + 3] = res[0] + res[1];
	}
}

/*
void matvec_XMM3(double* a, double* x, double* y, int n, int lb)
{
	int i, j;
	double *ptr_a0, *ptr_a1, *ptr_a2, *ptr_a3, *ptr_x;
	const int sj = 4, si = 4, s = si*sj;
	__m256d ra0, ra1, ra2, ra3, rx, ry0, ry1, ry2, ry3;
	__declspec(align(32)) double res[4];

	if (lb%s != 0)
	{
		cout << "lb % s != 0\n";
		system("pause");
		exit(1);
	}

	for (i = 0; i < n; i += si)
	{
		ptr_a0 = &a[i*n];
		ptr_a1 = &a[(i + 1)*n];
		ptr_a2 = &a[(i + 2)*n];
		ptr_a3 = &a[(i + 3)*n];

		ry0 = _mm256_setzero_pd();
		ry1 = _mm256_setzero_pd();
		ry2 = _mm256_setzero_pd();
		ry3 = _mm256_setzero_pd();

		for (j = 0, ptr_x = x; j < n; j += sj, ptr_a0 += sj, ptr_a1 += sj, ptr_a2 += sj, ptr_a3 += sj, ptr_x += sj)
		{
			ra0 = _mm256_load_pd(ptr_a0);
			ra1 = _mm256_load_pd(ptr_a1);
			ra2 = _mm256_load_pd(ptr_a2);
			ra3 = _mm256_load_pd(ptr_a3);

			rx = _mm256_load_pd(ptr_x);

			ra0 = _mm256_mul_pd(ra0, rx);
			ra1 = _mm256_mul_pd(ra1, rx);
			ra2 = _mm256_mul_pd(ra2, rx);
			ra3 = _mm256_mul_pd(ra3, rx);

			ry0 = _mm256_add_pd(ry0, ra0);
			ry1 = _mm256_add_pd(ry1, ra1);
			ry2 = _mm256_add_pd(ry2, ra2);
			ry3 = _mm256_add_pd(ry3, ra3);
		}

		_mm256_store_pd(res, ry0);
		y[i] = res[0] + res[1] + res[2] + res[3];
		_mm256_store_pd(res, ry1);
		y[i + 1] = res[0] + res[1] + res[2] + res[3];
		_mm256_store_pd(res, ry2);
		y[i + 2] = res[0] + res[1] + res[2] + res[3];
		_mm256_store_pd(res, ry3);
		y[i + 3] = res[0] + res[1] + res[2] + res[3];
	}
}
*/