
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import pl.jrj.game.IGameRemote;

/**
 * Main class
 * Reads data from URI and determines if student's grade is above, below
 * or equal to median from all grades from a given course
 * @author Rafa Zbojak
 */

@Stateless
@Path("/")
public class Service {
    
    private static List<Integer> marks = new ArrayList<Integer>();
    private static int studentMark;
    
    @PersistenceContext(unitName = "persistence93514")
    private EntityManager em;
    
    @EJB(mappedName = 
            "java:global/ejb-project/GameMonitor!pl.jrj.game.IGameRemote")
    IGameRemote monitor;
    
    /**
     * Main method
     * @param mediana name of the service
     * @param firstName student's first name
     * @param lastName student's last name
     * @param courseName name of the course
     * @return 
     */
    @GET
    @Path("{mediana}")
    public Integer getMediana(
            @PathParam("mediana") String mediana,
            @MatrixParam("firstName") String firstName,
            @MatrixParam("lastName") String lastName,
            @MatrixParam("courseName") String courseName
    ){
        int out = 0;
        if (monitor.register(8, "93514")) {
            getData(courseName,firstName, lastName);
            double median = calculateMedian();
                    
            out = assess(median, studentMark);
        }
        
        return out;
    }
    
    /**
     * Calculates median from grades.
     * First sorts list, then determines index of center element.
     * If number of grades is even, then returns mean from two center elements
     * @return calculated median
     */
    private double calculateMedian(){
        Collections.sort(marks);
        double median;
        int samples = marks.size();
        int index;
        
        if(samples % 2 == 0){
            index = samples / 2;
            median = ( marks.get(index - 1) + marks.get(index) ) / 2.0;
        }else{
            index = ( samples - 1 ) / 2;
            median = marks.get(index);
        }
        
        return median;
    }  
    
    /**
     * Determines if student's grade is above, below or equal to median
     * @param median
     * @param studentMark
     * @return 2 - above; 0 - below; 1 - equal to
     */
    private int assess(double median, int studentMark){
        if( studentMark > median ) {
            return 2;
        }else if( studentMark < median ) {
            return 0;
        }
        return 1;
    }
    
    /**
     * Gets grades from database and stores it into a list.
     * @param courseName name of the course
     * @param firstName student's first name
     * @param lastName student's last name
     */
    private void getData(String courseName, 
            String firstName, String lastName ) {
        TblCourses course = 
                em.createNamedQuery("findCourse", TblCourses.class).
                    setParameter("courseName", courseName).getSingleResult();
        
        String name, surname;
        int mark;
        
        for(TblStudentCourse sc : course.getStudents()){
            name = sc.getStudent().getFirstName();
            surname = sc.getStudent().getLastName();
            mark = sc.getMark();
            
            if( name.equals(firstName) && surname.equals(lastName) ){
                studentMark = mark;
            }
            marks.add(mark);
        }
    }
}
