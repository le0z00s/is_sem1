
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@NamedQuery(
    name="findCourse",
    query="SELECT i FROM TblCourses i WHERE i.courseName = :courseName"
)
@Table(name = "Tbl_Courses")
public class TblCourses implements Serializable{

    private static final long serialVersionUID = -1099703235608839862L;
    
    @Id
    @Column(name = "Id")
    private long id;

    @Column(name = "courseName")
    private String courseName;
    
    @OneToMany( mappedBy = "course" )
    private List<TblStudentCourse> students;

    /**
     * Get id
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Get course name
     *
     * @return course name
     */
    public String getCourseName() {
        return courseName;
    }
    
    /**
     * Get students
     *
     * @return student list
     */
    public List<TblStudentCourse> getStudents() {
        return students;
    }

    /**
     * Set id
     *
     * @param id student's id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Set course name
     *
     * @param courseName course name
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
    
    /**
     * Set students
     *
     * @param students List of students
     */
    public void setId(List<TblStudentCourse> students) {
        this.students = students;
    }

    /**
     * Default constructor
     */
    public TblCourses(){
        
    }

    /**
     * Create course with given id
     * @param id course id
     */
    public TblCourses(long id){
        this.id = id;
    }
    
    /**
     * Create course with given name and id
     * @param id course id
     * @param courseName course name
     * @param students list of students
     */
    public TblCourses(long id, String courseName, List<TblStudentCourse> students){
        this.id = id;
        this.courseName = courseName;
        this.students = students;
    }
    
}