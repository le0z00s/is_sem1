
import java.io.Serializable;

/**
 *
 * @author Rafał Zbojak
 */
public class StudentCourseId implements Serializable {
    
    private static final long serialVersionUID = -1099703235608839862L;

    private long studentId;

    private long courseId;

    /**
     * @return the studentId
     */
    public long getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the courseId
     */
    public long getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    @Override
    public int hashCode() {
        return (int) (getStudentId() + getCourseId());
    }

    @Override
    public boolean equals( Object object) {
        if( object instanceof StudentCourseId ) {
            StudentCourseId otherId = (StudentCourseId) object;
            return ( otherId.studentId == this.studentId )
                    && ( otherId.courseId == this.courseId );
        }
        return false;
    }
    
}
