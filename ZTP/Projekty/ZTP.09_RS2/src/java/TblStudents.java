
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table( name = "Tbl_Students" )
public class TblStudents implements Serializable{
    
    private static final long serialVersionUID = -1099703235608839862L;
    
    @Id
    @Column(name = "Id")
    private long id;

    @Column(name = "firstName")
    private String firstName;
    
    @Column(name = "lastName")
    private String lastName;
    
    @OneToMany( mappedBy = "student" )
    private List<TblStudentCourse> courses;

    /**
     * Get id
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Get first name
     *
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * Get last name
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }
    
    /**
     * Get courses
     *
     * @return courses list
     */
    public List<TblStudentCourse> getCourses() {
        return courses;
    }

    /**
     * Set id
     *
     * @param id student's id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Set first name
     *
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Set last name
     *
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    /**
     * Set courses
     *
     * @param courses List of courses
     */
    public void setId(List<TblStudentCourse> courses) {
        this.courses = courses;
    }

    /**
     * Default constructor
     */
    public TblStudents(){
        
    }

    /**
     * Create student with given id
     * @param id student id
     */
    public TblStudents(long id){
        this.id = id;
    }
    
    /**
     * Create student with given name and id
     * @param id student id
     * @param firstName  first name
     * @param lastName last name
     * @param courses  list of students
     */
    public TblStudents(long id, String firstName, String lastName, List<TblStudentCourse> courses){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.courses = courses;
    }
}
