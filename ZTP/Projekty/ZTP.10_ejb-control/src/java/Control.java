/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.jrj.game.IGameRemote;

/**
 *
 * @author Rafał Zbojak
 */
@WebServlet()
public class Control extends HttpServlet {

    @EJB(mappedName
            = "java:global/ejb-project/GameManager!pl.jrj.game.IGameRemote")
    IGameRemote monitor;
    @EJB
    IControlRemote control;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, 
            HttpServletResponse response)
            throws ServletException, IOException {

        if (monitor.register(5, "93514")) {
            Map<String, String[]> parameters = request.getParameterMap();
            if (parameters.containsKey("login")) {
                control.start();
            }
            if (parameters.containsKey("logout")) {
                control.stop();
            }
            if (parameters.containsKey("state")) {
                int increment;
                String state = request.getParameter("state");
                if (state.equals("")) {
                    increment = 1;
                } else {
                    increment = Integer.parseInt(state);
                }
                control.increment(increment);
            }
            if (parameters.containsKey("result")) {
                PrintWriter output = response.getWriter();
                int counter = control.counter();
                int errors = control.error();
                output.format("%d %d", counter, errors);
                output.close();
            }
        }

    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, 
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, 
            HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
