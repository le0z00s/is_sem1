
import javax.ejb.Stateless;

/**
 * Implementation of Control component
 * @author Rafał Zbojak
 */
@Stateless
public class ControlImpl implements IControlRemote{
    
    private boolean running;
    private int errors;
    private int counter;
    
    /**
     * Starts counting data
     */
    @Override
    public void start() {
        if(running){
            errors++;
        }else{
           running = true; 
        }
    }

    /**
     * Stops counting data
     */
    @Override
    public void stop() {
        if(!running){
            errors++;
        }else{
            running = false;
        }
    }

    /**
     * Increments counter by i.
     * If coutnting is disabled, 
     * increments error counter by 1.
     * @param i number by which to increment counter
     */
    @Override
    public void increment(int i) {
        if(running){
            counter +=i;
        }else{
            errors++;
        }
    }

    /**
     * Returns counter state
     * @return Counter state
     */
    @Override
    public int counter() {
        return counter;
    }

    /**
     * Return error counter state
     * @return Counter state
     */
    @Override
    public int error() {
        return errors;
    }
    
}
