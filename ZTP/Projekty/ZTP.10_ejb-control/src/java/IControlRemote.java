
import javax.ejb.Remote;

/**
 * Interface for <code>ControlImpl</code>
 * @author Rafał Zbojak
 */
@Remote
public interface IControlRemote {
    /**
     * Starts counting data
     */
    public void start();
    /**
     * Stops counting data
     */
    public void stop();
    /**
     * Increments counter by i.
     * If coutnting is disabled, 
     * increments error counter by 1.
     * @param i number by which to increment counter
     */
    public void increment(int i);
    /**
     * Returns counter state
     * @return Counter state
     */
    public int counter();
    /**
     * Return error counter state
     * @return Counter state
     */
    public int error();
}
