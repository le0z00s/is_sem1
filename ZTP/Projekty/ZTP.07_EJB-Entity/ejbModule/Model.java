import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Model entity bean
 * @author Rafał Zbojak
 */
@Entity
@Table ( name = "Tb_Model" )
public class Model implements Serializable{
	
	private static final long serialVersionUID = 8107940450767469798L;
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column ( name = "Id" )
	private int id;
	
	@Column ( name = "model" )
	private String model;
	
	/**
	 * Get model id
	 * @return model id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get model name
	 * @return model name
	 */
	public String getModel(){
		return model;
	}
	
	/**
	 * Default constructor
	 */
	public Model() {}
	
	/**
	 * Creates car with given id
	 * @param id model id
	 */
	public Model( int id ) {
		this.id = id;
	}
	
	/**
	 * Creates model entry with given parameters
	 * @param id model id
	 * @param model car model
	 */
	public Model( int id, String model ) {
		this.id = id;
		this.model = model;
	}
	
	/**
	 * Set model id
	 * @param id car id
	 */
	public void setId( int id ) {
		this.id = id;
	}
	
	/**
	 * Set model name
	 * @param model model name
	 */
	public void setModel( String model ) {
		this.model = model;
	}
	
}
