package pl.jrj.game;
import javax.ejb.Remote;

/**
 * Interface for GameMonitor session bean 
 * @author Rafał Zbojak
 */
@Remote
public interface IGameRemote {
	public boolean register(int hwork, String album);
} 