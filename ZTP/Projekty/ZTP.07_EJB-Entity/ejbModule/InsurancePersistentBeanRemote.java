import java.util.Date;
import java.util.List;

import javax.ejb.Local;


/**
 * Interface for InsurancePersistentBean
 * @author Rafał Zbojak
 */
@Local
public interface InsurancePersistentBeanRemote {
	
	/**
	 * Counts customers who had insurance
	 * for a car in the given time range
	 * @param model car model
	 * @param date1 starting date
	 * @param date2 ending date
	 * @param name1 starting name prefix
	 * @param name2 ending name prefix
	 * @return amount of clients meting query
	 */
	Integer count(String model, Date date1, Date date2, 
			String name1, String name2);
	
	/**
	 * Gets list of all insurances
	 * @return list of all insurances
	 */
	List<Insurance> getInsurances();
}
