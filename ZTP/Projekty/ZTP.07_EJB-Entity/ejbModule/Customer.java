import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Customer entity bean
 * @author Rafał Zbojak
 */
@Entity
@Table ( name = "Tb_Customer" )
public class Customer implements Serializable{

	private static final long serialVersionUID = -1099703235608839862L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column ( name = "Id" )
	private int id;
	
	@Column ( name = "firstName" )
	private String firstName;
	
	@Column ( name = "lastName" )
	private String lastName;

	/**
	 * Get customer id
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Get first name
	 * @return first name
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Get last name
	 * @return last name
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Default constructor
	 */
	public Customer() {}
	
	/**
	 * Create customer object with given id
	 * @param id customer id
	 */
	public Customer( int id ) {
		this.id = id;
	}
	
	/**
	 * Create customer entry with given parameters
	 * @param id customer id
	 * @param firstName first name
	 * @param lastName last name
	 */
	public Customer( int id, String firstName, String lastName ) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	/**
	 * Set customer id
	 * @param id customer id
	 */
	public void setId( int id ) {
		this.id = id;
	}
	
	/**
	 * Set customer's name
	 * @param firstName customer name
	 */
	public void setFirstName( String firstName ) {
		this.firstName = firstName;
	}
	
	/**
	 * Set customer's last name
	 * @param lastName last name
	 */
	public void setLastName( String lastName ) {
		this.lastName = lastName;
	}
}
