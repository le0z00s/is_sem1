import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class InsurancePersistentBean 
implements InsurancePersistentBeanRemote {

	@PersistenceContext( unitName = "persistence93514" )
	private EntityManager manager;


	/**
	 * Counts customers who had insurance
	 * for a car in the given time range
	 * @param model car model
	 * @param date1 starting date
	 * @param date2 ending date
	 * @param name1 starting name prefix
	 * @param name2 ending name prefix
	 * @return amount of clients meting query
	 */
	@Override
	public Integer count(String model, Date date1, Date date2, String name1,
			String name2) {

		int counter = 0;

		List<Insurance> list = getInsurances();
		boolean modelCheck = false;
		boolean customerCheck = false;
		boolean startDateCheck = false;
		boolean endDateCheck = false;
		
		for( Insurance insurance : list ) {
			modelCheck = insurance.getModel()
					.getModel().equalsIgnoreCase(model);
			customerCheck = stringInRange(name1, name2, 
					insurance.getCustomer().getLastName());
			startDateCheck = !insurance.getDateFrom().after(date1);
			endDateCheck = !insurance.getDateTo().before(date2);
			
			if(modelCheck && customerCheck 
					&& startDateCheck && endDateCheck) {
				++counter;
			}
		}

		return counter;
	}

	/**
	 * Utility method checking if given string 
	 * is within a range
	 * @param lowerBound prefix set as lower bound
	 * @param upperBound prefix set as upper bound
	 * @param input input string to check
	 * @return true if input is within a range
	 */
	private boolean stringInRange( String lowerBound, 
			String upperBound, String input ) {

		int compLower = input.compareToIgnoreCase(lowerBound);
		int compUpper = input.compareToIgnoreCase(upperBound);

		if( compLower >= 0 && compUpper <= 0 ){
			return true;
		}

		return false;
	}

	/**
	 * Gets list of all insurances
	 * @return list of all insurances
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Insurance> getInsurances() {

		List<Insurance> out = manager
				.createQuery("SELECT i FROM Insurance i")
				.getResultList();
		return out;
	}
}
