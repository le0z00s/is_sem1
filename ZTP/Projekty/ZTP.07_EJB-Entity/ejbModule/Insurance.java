import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Insurance entity bean
 * @author Rafał Zbojak
 */
@Entity
@Table( name = "Tb_Insurance" )
public class Insurance {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "Id" )
	private int id;
	
	@Column( name = "dateFrom" )
	private Date dateFrom;
	
	@Column( name = "dateTo" )
	private Date dateTo;
	
	@JoinColumn( name = "customerId", referencedColumnName = "Id" )
	@ManyToOne( optional = false )
	private Customer customer;
	
	@JoinColumn( name = "modelId", referencedColumnName = "Id" )
	@ManyToOne( optional = false )
	private Model model;
	
	/**
	 * Default constructor
	 */
	public Insurance() {}
	
	/**
	 * Creates insurance entry with given id
	 * @param id insurance id
	 */
	public Insurance( int id ) {
		this.id = id;
	}
	
	/**
	 * Creates insurance entry with given parameters
	 * @param id insurance id
	 * @param customer customer id
	 * @param model model id
	 * @param dateFrom starting date
	 * @param dateTo ending date
	 */
	public Insurance( int id, Customer customer, Model model, Date dateFrom, Date dateTo) {
		this.id = id;
		this.customer = customer;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.model = model;
	}
	
	/**
	 * Get insurance id
	 * @return insurance id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Set insurance id
	 * @param id insurance id
	 */
	public void setId( int id ) {
		this.id = id;
	}
	
	/**
	 * Get starting date
	 * @return starting date
	 */
	public Date getDateFrom() {
		return dateFrom;
	}
	
	/**
	 * Set starting date
	 * @param dateFrom starting date
	 */
	public void setDateFrom( Date dateFrom ) {
		this.dateFrom = dateFrom;
	}
	
	/**
	 * Get end date
	 * @return end date
	 */
	public Date getDateTo() {
		return dateTo;
	}
	
	/**
	 * Set end date	
	 * @param dateTo end date
	 */
	public void setDateTo( Date dateTo ) {
		this.dateTo = dateTo;
	}
	
	/**
	 * Get referenced customer
	 * @return customer
	 */
	public Customer getCustomer(){
		return customer;
	}
	
	/**
	 * Set referenced customer
	 * @param customer customer id
	 */
	public void setCustomer ( Customer customer ) {
		this.customer = customer;
	}
	
	/**
	 * Get model reference
	 * @return model id
	 */
	public Model getModel() {
		return model;
	}
	
	/**
	 * Set reference to a model
	 * @param model model id
	 */
	public void setModel( Model model ) {
		this.model = model;
	}
}
