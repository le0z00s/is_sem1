import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;

import pl.jrj.game.IGameRemote;

/**
 * Main class. Counts clients who
 * had insurance for a specific car model
 * in the given time range
 * @author Rafał Zbojak
 *
 */
public class Count {

	@EJB( mappedName = "java:global/ejb-project/GameMonitor!IGameRemote" )
	private static IGameRemote game;
	
	@EJB
	private static InsurancePersistentBeanRemote persistance;

	/**
	 * Main method. Registers user, reads file
	 * and passes results to InsurancePersistentBean which
	 * counts customers
	 * @param args
	 */
	public static void main(String[] args){
		int out = -2;
		try{
			if( game.register( 7, "93514" )) {
				if( args.length > 0 ) {
					HashMap<String, String> params = readFile(args[0]);
					
					String model = params.get("model");
					Date date1 = convertDate(params.get("date1"));
					Date date2 = convertDate(params.get("date2"));
					String name1 = params.get("name1");
					String name2 = params.get("name2");
					
					persistance = new InsurancePersistentBean();
					out = persistance.count(model, date1, date2, name1, name2);
				}
			}
		} catch( Exception e ) {
			out = -1;
		}
		System.out.print(out);
	}

	/**
	 * Reads input file and saves it as HashMap containing
	 * car model, dates and name parts for query
	 * @param file Path for a file
	 * @return Map containing parameters for query
	 * @throws IOException 
	 */
	private static HashMap<String, String> readFile( String file ) 
			throws IOException {
		List<String> lines;
		HashMap<String, String> out = new HashMap<String, String>();

		Path path = Paths.get(file);
		Charset cs = StandardCharsets.UTF_8;
		lines = Files.readAllLines( path, cs );
		String regex = "\\s+";		

		out.put("model", lines.get(0));

		String[] tmp = lines.get(1).split(regex);
		out.put("date1", tmp[0]);
		out.put("date2", tmp[1]);

		tmp = lines.get(2).split(regex);
		out.put("name1", tmp[0]);
		out.put("name2", tmp[1]);

		return out;
	}
	
	/**
	 * Convert String dd/MM/yyyy to Date object
	 * @param dateString String to parse
	 * @return Parsed Date object
	 * @throws ParseException
	 */
	private static Date convertDate(String dateString) 
			throws ParseException {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date out = format.parse(dateString);
		return out;
	}

}
