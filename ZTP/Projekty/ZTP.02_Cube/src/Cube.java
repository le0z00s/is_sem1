import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Cube extends HttpServlet {

	ArrayList<Grain> grains = new ArrayList<Cube.Grain>();

	/**
	 * Method connects to database and retrieve data with location and size of grain
	 * @param database String for connecting to the database using JDBC
	 * @throws SQLException
	 */
	private void connectDB(String database){
		double x,y,z,r;
		Connection conn;
		try {
			conn = DriverManager.getConnection(database);

			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Etable");

			while(resultSet.next()){
				x = resultSet.getDouble(2);
				y = resultSet.getDouble(3);
				z = resultSet.getDouble(4);
				r = resultSet.getDouble(5);

				grains.add(new Grain(x, y, z, r));
			}

			resultSet.close();
			statement.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method generates a random sample between 0 and n 
	 * @param range Size of cube edge
	 */
	private double getRandom(double range){
		SecureRandom random = new SecureRandom();
		double sample = random.nextDouble()*range;
		return sample;
	}

	/**
	 * Method checks if the point belongs to grain
	 * @return true if point belongs to grain, false if not 
	 */
	private boolean isGrain(double x, double y, double z){
		for(Grain grain : grains){
			if(Math.pow((x-grain.getX()), 2)+Math.pow((y-grain.getY()), 2)+Math.pow((z-grain.getZ()), 2)<=Math.pow(grain.getR(), 2)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Method returns ratio of grain hits to all hits
	 * @param size Length of the cube's edge
	 * @return ratio of number of grain hits to all hits
	 */
	private double monteCarlo(double size){
		int grainHit = 0;
		int precision = 1000000;
		double x, y, z;
		
		for(int i = 0; i < precision; i++){
			x = getRandom(size);
			y = getRandom(size);
			z = getRandom(size);
			
			if(isGrain(x, y, z)){
				grainHit++;
			}
		}
		
		double ratio = (double) grainHit/precision;

		return ratio;
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		PrintWriter printWriter = response.getWriter();
		response.setContentType("text/html");

		double size = Double.parseDouble(request.getParameter("a"));
		double density = Double.parseDouble(request.getParameter("b"));
		String connectionString = request.getParameter("db");
		connectDB(connectionString);
		
		double grainRatio = monteCarlo(size);
		double matRatio = 1.0f - grainRatio;
		double cubeVolume = Math.pow(size, 3);
		
		double mass = density*cubeVolume*(matRatio + 1.2f*grainRatio);
		
		printWriter.format(Locale.US, "%.3f", mass);
		printWriter.close();
	}

	/**
	 * Helper class representing impurity in the material
	 */
	class Grain{
		private double x, y, z, r;

		public Grain(double x, double y, double z, double r){

		}

		public double getX () {
			return x;
		}

		public double getY () {
			return y;
		}

		public double getZ () {
			return z;
		}

		public double getR () {
			return r;
		}
	}
}
