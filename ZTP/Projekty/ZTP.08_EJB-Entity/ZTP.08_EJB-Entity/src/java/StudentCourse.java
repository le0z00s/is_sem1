
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table( name = "Tbl_Student_Course" )
@IdClass(StudentCourseId.class)
public class StudentCourse implements Serializable {
    
    private static final long serialVersionUID = -1099703235608839862L;
    
    @Id
    private long studentId;
    
    @Id
    private long courseId;
    
    @Column( name = "mark" )
    private int mark;
    
    @ManyToOne
    @PrimaryKeyJoinColumn( name = "studentId", referencedColumnName = "Id" )
    private StudentEntity student;
    
    @ManyToOne
    @PrimaryKeyJoinColumn( name = "courseId", referencedColumnName = "Id" )
    private CourseEntity course;

    /**
     * @return the studentId
     */
    public long getStudentId() {
        return studentId;
    }

    /**
     * @return the courseId
     */
    public long getCourseId() {
        return courseId;
    }

    /**
     * @return the mark
     */
    public int getMark() {
        return mark;
    }

    /**
     * @return the student
     */
    public StudentEntity getStudent() {
        return student;
    }

    /**
     * @return the course
     */
    public CourseEntity getCourse() {
        return course;
    }
    
    /**
     * Default constructor
     */
    public StudentCourse(){
    
    }
}
