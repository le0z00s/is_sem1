
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table( name = "Tbl_Students" )
public class StudentEntity implements Serializable{
    
    @Id
    @Column(name = "Id")
    private long id;

    @Column(name = "firstName")
    private String firstName;
    
    @Column(name = "lastName")
    private String lastName;
    
    @OneToMany( mappedBy = "student" )
    private List<StudentCourse> courses;

    /**
     * Get id
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Get first name
     *
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * Get last name
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }
    
    /**
     * Get courses
     *
     * @return courses list
     */
    public List<StudentCourse> getCourses() {
        return courses;
    }

    /**
     * Set id
     *
     * @param id student's id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Set first name
     *
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Set last name
     *
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    /**
     * Set courses
     *
     * @param courses List of courses
     */
    public void setId(List<StudentCourse> courses) {
        this.courses = courses;
    }

    /**
     * Default constructor
     */
    public StudentEntity(){
        
    }

    /**
     * Create student with given id
     * @param id student id
     */
    public StudentEntity(long id){
        this.id = id;
    }
    
    /**
     * Create student with given name and id
     * @param id student id
     * @param firstName  first name
     * @param lastName last name
     * @param courses  list of students
     */
    public StudentEntity(long id, String firstName, String lastName, List<StudentCourse> courses){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.courses = courses;
    }
}
