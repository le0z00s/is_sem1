
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.jrj.game.IGameRemote;

/**
 * Main class
 * Reads data from file and determines if student's grade is above, below
 * or equal to median from all grades from a given course
 * @author Rafał Zbojak
 */
public class Student {

    private EntityManager em;
    
    private List<Integer> marks;
    private int studentMark;

    public Student(){
        this.marks = new ArrayList<Integer>();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String beanString
                = "java:global/ejb-project/GameMonitor!pl.jrj.game.IGameRemote";
        InitialContext ctx;
        int out = 0;
        try {
            ctx = new InitialContext();
            IGameRemote monitor = (IGameRemote) ctx.lookup(beanString);

            if (monitor.register(8, "93514")) {
                if (args.length > 0) {
                    Student student = new Student();
                    
                    HashMap<String, String> params = student.readfile(args[0]);
                    
                    String courseName = params.get("courseName");
                    String firstName = params.get("firstName");
                    String lastName = params.get("lastName");
                    
                    student.getData("myPersistence", courseName, 
                            firstName, lastName);
                    
                    double median = student.calculateMedian();
                    
                    out = student.assess(median, student.studentMark);
                }
            }
            
        } catch (Exception ex) {
            Logger.getLogger(Student.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        
        System.out.print(out);
    }

    /**
     * Reads input file and saves it as HashMap containing course name
     * and student's personal data
     *
     * @param file Path for a file
     * @return Map containing parameters for query
     * @throws IOException
     */
    private HashMap<String, String> readfile(String file) 
            throws IOException{
        List<String> lines;
        HashMap<String, String> out = new HashMap<String, String>();
        
        Path path = Paths.get(file);
        Charset cs = StandardCharsets.UTF_8;
        lines = Files.readAllLines(path, cs);
        
        String regex = "\\s+";
        
        out.put("courseName", lines.get(0).trim());
        
        String[] tmp = lines.get(1).split(regex);
        out.put("firstName", tmp[0].trim());
        out.put("lastName", tmp[1].trim());
        
        return out;
    }
    
    /**
     * Calculates median from grades.
     * First sorts list, then determines index of center element.
     * If number of grades is even, then returns mean from two center elements
     * @return calculated median
     */
    private double calculateMedian(){
        Collections.sort(this.marks);
        double median;
        int samples = marks.size();
        int index;
        
        if(samples % 2 == 0){
            index = samples / 2;
            median = ( marks.get(index - 1) + marks.get(index) ) / 2.0;
        }else{
            index = ( samples - 1 ) / 2;
            median = marks.get(index);
        }
        
        return median;
    }
    
    /**
     * Determines if student's grade is above, below or equal to median
     * @param median
     * @param studentMark
     * @return 1 - above; -1 - below; 0 - equal to
     */
    private int assess(double median, int studentMark){
        if( studentMark > median ) {
            return 1;
        }else if( studentMark < median ) {
            return -1;
        }
        return 0;
    }
    
    /**
     * Gets grades from database and stores it into a list.
     * @param unitName JPA unit name
     * @param courseName name of the course
     * @param firstName student's first name
     * @param lastName student's last name
     */
    private void getData( String unitName, String courseName, 
            String firstName, String lastName ) {
        EntityManagerFactory emf = Persistence.
                createEntityManagerFactory(unitName);
        this.em = emf.createEntityManager();
        
        CourseEntity course = 
                em.createNamedQuery("findCourse", CourseEntity.class).
                    setParameter("courseName", courseName).getSingleResult();
        
        String name, surname;
        int mark;
        
        for(StudentCourse sc : course.getStudents()){
            name = sc.getStudent().getFirstName();
            surname = sc.getStudent().getLastName();
            mark = sc.getMark();
            
            if( name.equals(firstName) && surname.equals(lastName) ){
                this.studentMark = mark;
            }
            
            this.marks.add(mark);
        }
    }
}