import javax.ejb.Stateless;
import pl.jrj.game.IGameRemote;

/**
 * Przykłądowa implementacja do testowania metody lookup
 *
 * @author Rafał Zbojak
 */
@Stateless
public class GameMonitor implements IGameRemote {

    @Override
    public boolean register(int hwork, String album) {
        return true;
    }
}
