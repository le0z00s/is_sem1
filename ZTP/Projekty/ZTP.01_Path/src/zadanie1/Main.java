package zadanie1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import zadanie1.Path.DirectedGraph;
import zadanie1.Path.Edge;

public class Main {

	private static final String query = "SELECT x,y,p FROM Gtable";
	private static int v = 0;
	
	public static List<Edge> connect(final String database){
		List<Edge> edges = null;
		
		try{
			Connection conn = DriverManager.getConnection(database);
			Statement statement = conn.createStatement();
			edges = retriveData(statement.executeQuery(query));
			statement.close();
            conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return edges;
	}
	
	public static List<Edge> retriveData(final ResultSet result) throws SQLException{
		List<Edge> edges = new ArrayList<Edge>();
		while(result.next()){
			int x = result.getInt("x");
			int y = result.getInt("y");
			float p = result.getFloat("p");
			Edge edge = new Edge(x, y, p);
			v = Math.max(v, Math.max(x, y));
			edges.add(edge);
		}
		return edges;
	}
	
	public static void main(final String[] args){
		try{
			List<Edge> edges = connect(args[0]);
			
			DirectedGraph graph = new DirectedGraph(v+1);
			
			for(Edge edge : edges){
				graph.addEdge(edge);
			}
			
			Path path = new Path(graph, 1);
			int to = Integer.parseInt(args[1]);
			
			if(path.hasPath(to)){
				printCost(path.getOveralCost());
			}else{
				printCost(0f);
			}
		}catch(Exception e){
			printCost(0f);
		}
	}
	
	private static void printCost(float cost){
		System.out.format(Locale.US, "%.3f", cost);
	}
		
}
