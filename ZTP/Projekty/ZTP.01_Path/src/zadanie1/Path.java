package zadanie1;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Class is using Dijkstra algorithm for directed graph
 * @author Rafał Zbojak
 */
public class Path {

	public static class Edge{
		private int x, y;
		private float p;
		
		//setters and getters for edge variables		
		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public float getP() {
			return p;
		}

		public void setP(float p) {
			this.p = p;
		}
		
		/**
		 * @param x edge starting point
		 * @param y edge ending point
		 * @param p maximum throughput over the edge
		 */
		public Edge(int x, int y, float p){
			setX(x);
			setY(y);
			setP(p);
		}
	}
	
	public static class DirectedGraph{
		private int numberOfVertexes;
		private int numberOfEdges;
		
		//Neighbourhood list for all vertexes in graph
		private List<Edge>[] neighborhoodList;
		
		@SuppressWarnings("unchecked")
		public DirectedGraph(int numberOfVertexes){
			this.setNumberOfVertexes(numberOfVertexes);
			this.setNumberOfEdges(0);
			this.neighborhoodList = (List<Edge>[]) new List[numberOfVertexes];
			for(int i=0; i<numberOfVertexes; i++){
				neighborhoodList[i] = new ArrayList<Edge>();
			}
		}
		
		//setters and getters for fields
		public int getNumberOfVertexes() {
			return numberOfVertexes;
		}

		public void setNumberOfVertexes(int numberOfVertexes) {
			this.numberOfVertexes = numberOfVertexes;
		}

		public int getNumberOfEdges() {
			return numberOfEdges;
		}

		public void setNumberOfEdges(int numberOfEdges) {
			this.numberOfEdges = numberOfEdges;
		}
		
		/**
		 * Adds directed edge to the neighbourhood list of a digraph
		 * @param edge - edge that will be added
		 */
		public void addEdge(Edge edge){
			neighborhoodList[edge.getX()].add(edge);
			numberOfEdges++;
		}
		
		/**
		 * Returns neighborhood list for a given vertex
		 * @param v - vertex index in a neighborhood list
		 * @return - iterable collection of directed edges
		 */
		public Iterable<Edge> getNeighborhoodList(int v){
			return neighborhoodList[v];
		}
	}
	
	public class Cost implements Comparable<Cost>{
		private final int edge;
		private float cost;
		
		public Cost(int edge, float cost){
			this.edge = edge;
			this.setCost(cost);
		}

		public float getCost() {
			return cost;
		}

		public void setCost(float cost) {
			this.cost = cost;
		}

		public int getEdge() {
			return edge;
		}

		@Override
		public int compareTo(Cost o) {
			int cmp = new Float(cost).compareTo(o.getCost());

			if (cmp == 0) {
				return new Integer(edge).compareTo(o.getEdge());
			}
			return 0;
		}
		
		
	}
	
	private Edge[] edgeTo;
	private Float[] throughputTo;
	private Float overalCost=0f;
	private Queue<Cost> queue;
	
	//relax graph
	private void relax(DirectedGraph graph, int v) {
		// iterate through the neighborhood list
		for (Edge edge : graph.getNeighborhoodList(v)) {
			int w = edge.getY();

			if (throughputTo[w] > throughputTo[v] + edge.getP()) {
				throughputTo[w] = throughputTo[v] + edge.getP();
				edgeTo[w] = edge;
				Cost cost = new Cost(w, 1/throughputTo[w]);
				overalCost+=cost.getCost();
				
				queue.remove(cost);
				queue.offer(cost);
			}
		}
	}
	
	public float getThroughputTo(int v){
		return throughputTo[v];
	}
	
	public boolean hasPath(int v){
		return throughputTo[v] < Float.MAX_VALUE;
	}
	
	public Path(DirectedGraph graph, int source){
		edgeTo = new Edge[graph.getNumberOfVertexes()];
		throughputTo = new Float[graph.getNumberOfVertexes()];
		queue = new PriorityQueue<Cost>(graph.getNumberOfVertexes());
		
		for (int v = source; v < graph.getNumberOfVertexes(); v++) {
			throughputTo[v] = Float.MAX_VALUE;
		}
		
		throughputTo[source] = 0f;
		
		queue.offer(new Cost(source, 0f));

		while (!queue.isEmpty()) {
			relax(graph, queue.poll().getEdge());
		}
	}

	public Float getOveralCost() {
		return overalCost;
	}

	public void setOveralCost(Float overalCost) {
		this.overalCost = overalCost;
	}
}
