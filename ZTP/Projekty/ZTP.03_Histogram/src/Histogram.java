import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest;

/**
 * Main class. Performs Kolmogorov-Smirnov test for two histograms.
 * @author Rafal Zbojak
 */
public class Histogram extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Method collects samples and puts them into dataset
	 * @param request - request information from HTTP servlet
	 * @param response - response information from HTTP servlet
	 * @throws ServletException
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		response.setContentType("text/plain");

		int n = Integer.parseInt(request.getParameter("n"));

		Data data = Data.getInstance();
		data.add(n);
		printWriter.close();
	}

	/**
	 * Method collects samples and puts them into dataset
	 * @param request - request information from HTTP servlet
	 * @param response - response information from HTTP servlet
	 * @throws ServletException
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		response.setContentType("text/plain");

		double m = Double.parseDouble(request.getParameter("m"));
		double s = Double.parseDouble(request.getParameter("s"));

		s = Math.sqrt(s);

		KolmogorovSmirnovTest test = new KolmogorovSmirnovTest();
		NormalDistribution dist = new NormalDistribution(m, s);

		Data data = Data.getInstance();

		double[] x = getTestData(data.getData(), data.getSamplesAmount());
		
		double p = test.kolmogorovSmirnovTest(dist, x);

		data.clear();
		printWriter.println(p<=0.05 ? 0 : 1);
		printWriter.close();
	}

	/**
	 * Method converting dataset to double array
	 * @param dataset - map containing tested data
	 * @param size - amount of samples in the dataset
	 * @return sorted double array containing collected data
	 */
	public double[] getTestData(Map<Integer, Integer> dataset, int size){
		int i=0;
		int value;
		double[] out = new double[size];

		for (Map.Entry<Integer, Integer> entry : dataset.entrySet()){
			value = entry.getValue();
			for (int j=0; j<value; j++){
				out[i] = entry.getKey();
				i++;
			}
		}

		return out;
	}

	/**
	 * Class storing map representation of histogram
	 * @author Rafal Zbojak
	 */
	public static class Data{
		private static final Data instance = new Data();
		private Map<Integer, Integer> data = new TreeMap<Integer, Integer>();
		private int samples = 0;
		
		protected Data(){}

		public static Data getInstance(){
			return instance;
		}

		/**
		 * Returns map containing collected samples
		 * @return data - map containing collected samples and their amount
		 */
		public Map<Integer, Integer> getData(){
			return data;
		}

		/**
		 * Adds sample to the dataset
		 * @param n - sample number
		 */
		public void add(int n){
			int value = data.get(n) == null ? 0 : data.get(n);
			data.put(n, value+1);
			samples++;
		}

		/**
		 * Clears dataset
		 */
		public void clear(){
			data.clear();
			samples = 0;
		}

		/**
		 * Get overall amount of samples in the dataset
		 * @return samples - amount of samples in the dataset
		 */
		public int getSamplesAmount(){
			return samples;
		}
	}
}
