package pl.jrj.game;
import javax.ejb.Remote;

/**
 * @author Rafal Zbojak
 *	@version 1.0
 */

@Remote
public interface IGameRemote {
	/**
	 * @param hwork Task number
	 * @param album Students album number
	 * @return True if registration is successful
	 */
	public boolean register(int hwork, String album); // hwork - numer zadania, album – numer albumu studenta
}