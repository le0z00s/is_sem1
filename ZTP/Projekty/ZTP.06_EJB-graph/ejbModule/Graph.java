import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

/**
 * @author Rafal Zbojak
 * @version 1.0
 */

@Stateless
public class Graph implements IGraphRemote{
	private List<Integer>[] adj;
	private boolean[] checked;
	
	/**
	 * Default constructor
	 */
	public Graph() {}
	
	/**
	 * Initializes a graph of a given size
	 * @param size Size of a graph
	 */
	public Graph( int size){
		adj = new List[size];
		init();
		
		for( int i = 0; i < size; i++){
			adj[i] = new ArrayList<Integer>();
		}
	}
	
	/**
	 * Adds an edge to the graph
	 * @param A Vertex A
	 * @param B Vertex B
	 */
	@Override
	public void addEdge(int A, int B) {
		if (!adj[A].contains(B)){
			adj[A].add(B);
			adj[B].add(A);
		}
	}

	/**
	 * Gets vertices adjacent to vertex V
	 * @param v Vertex V
	 * @return List of adjacent vertices
	 */
	@Override
	public List<Integer> getAdjacentVertex(int v) {
		return adj[v];
	}

	/**
	 * Returns size of a graph
	 * @return Size of a graph
	 */
	@Override
	public int size() {
		return adj.length;
	}

	/**
	 * Check if given vertex was processed
	 * @param v Vertex V
	 * @return True if vertex was processed
	 */
	@Override
	public boolean getChecked(int v) {
		return checked[v];
	}

	/**
	 * Sets vertex as checked
	 * @param v Vertex V
	 */
	@Override
	public void setChecked(int v) {
		checked[v] = true;
	}

	/**
	 * Initialize vertices for DFS setting them as unchecked
	 */
	@Override
	public void init() {
		checked = new boolean[size()];
	}

}
