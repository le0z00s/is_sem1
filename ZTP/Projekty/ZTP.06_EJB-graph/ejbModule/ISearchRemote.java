import java.util.Set;

import javax.ejb.Local;


/**
 * @author Rafal Zbojak
 * @version 1.0
 */
@Local
public interface ISearchRemote {
	/**
	 * Executes deep first search for a graph.
	 * To ensure that there are no duplicate paths
	 * paths are saved as sorted sets
	 * @param graph Graph that will be traversed
	 * @return Set of compact parts of a graph
	 */
	Set<Set<Integer>> executeSearch (Graph graph);
}
