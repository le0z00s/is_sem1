import java.util.List;

import javax.ejb.Local;

/**
 * @author Rafal Zbojak
 * @version 1.0
 */
@Local
public interface IGraphRemote {
	
	/**
	 * Adds an edge to the graph
	 * @param A Vertex A
	 * @param B Vertex B
	 */
	void addEdge (int A, int B);
	
	/**
	 * Gets vertices adjacent to vertex V
	 * @param v Vertex V
	 * @return List of adjacent vertices
	 */
	List<Integer> getAdjacentVertex(int v);
	
	/**
	 * Returns size of a graph
	 * @return Size of a graph
	 */
	int size();
	
	/**
	 * Check if given vertex was processed
	 * @param v Vertex V
	 * @return True if vertex was processed
	 */
	boolean getChecked(int v);
	
	/**
	 * Sets vertex as checked
	 * @param v Vertex V
	 */
	void setChecked (int v);
	
	/**
	 * Initialize vertices for DFS setting them as unchecked
	 */
	void init();
	
}
