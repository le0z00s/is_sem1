import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.ejb.EJB;

import pl.jrj.game.IGameRemote;

/**
 * Main application class. 
 * Reads data from input file and executes calculations.
 * @author Rafal Zbojak
 * @version 1.0
 */
public class AppClient {

	@EJB(mappedName = "java:global/ejb-project/GameMonitor!IGameRemote")
	private static IGameRemote game;

	@EJB
	private  static IGraphRemote graph;

	@EJB
	private static ISearchRemote search;

	/**
	 * Main program method
	 * @param args Input file path
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args){
		int componentCount = 0;
		try{
			if(game.register(6, "93514")){

				Set<Set<Integer>> result = new HashSet<Set<Integer>>();
				List<Integer> edges = new ArrayList<Integer>();

				edges = readData(args[0]);

				// get the last element of edges list and increment it by 1
				int maxElement = Collections.max(edges);

				graph = new Graph(maxElement + 1);

				// set size condition for loop 
				//ensuring that number of elements is % 2
				int size = checkDataSize(edges);

				//build graph
				for(int i = 0; i < size; i += 2){
					graph.addEdge(edges.get(i), edges.get(i + 1));
				}

				search = new Search();
				result = search.executeSearch((Graph) graph);
				
				componentCount = result.size();
			}
		}catch ( Exception e){
			componentCount = 0;
		}
		System.out.print(componentCount);

	}

	/**
	 * Checks if input data consists of even number of elements
	 * @param edgeList List of vertices
	 * @return Size of list rounded down to nearest multiple of 2
	 */
	private static int checkDataSize(List<Integer> edgeList){
		int size = edgeList.size();
		return size - size % 2;
	}

	/**
	 * Reads data from input file and 
	 * @param inputFile
	 * @return List of edges where two following elements
	 * defines an edge
	 * @throws FileNotFoundException
	 */
	private static List<Integer> readData(String inputFile) 
			throws FileNotFoundException{
		List<Integer> edges = new ArrayList<Integer>();
		Scanner scanner = new Scanner(new File(inputFile));
		while (scanner.hasNextInt()){
			edges.add(scanner.nextInt());
		}
		scanner.close();
		return edges;
	}
}
