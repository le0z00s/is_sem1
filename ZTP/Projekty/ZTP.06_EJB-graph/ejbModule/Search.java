import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;


/**
 * @author Rafal Zbojak
 * @version 1.0
 */
public class Search implements ISearchRemote{
	
	/**
	 * Default constructor.
	 */
	public Search() {}


	@Override
	public Set<Set<Integer>> executeSearch(Graph graph) {
		Set<Set<Integer>> result =  new HashSet<Set<Integer>>();
		
		for ( int i = 0; i < graph.size(); i++){
			// initialize new search
			graph.init();
			
			Set<Integer> component = new TreeSet<Integer>();
			
			// recursively traverse the graph from a 'i' vertex
			checkNextVertex(graph, i);
			
			// add path to the list
			for (int j = 0; j < graph.size(); j++){
				if(graph.getChecked(j)){
					component.add(j);
				}
			}
			result.add(component);
		}
		return result;	
	}
	
	/**
	 * Traverse the graph from given vertex.
	 * Method is running recursively until no adjacent vertex is found
	 * @param graph Graph to traverse
	 * @param v Index of a current vertex
	 */
	private void checkNextVertex(Graph graph, int v){
		graph.setChecked(v); // set vertex as processed
		
		for (Integer x : graph.getAdjacentVertex(v)){
			if( !graph.getChecked(x)){
				checkNextVertex(graph, x);
			}
		}
	}

}
