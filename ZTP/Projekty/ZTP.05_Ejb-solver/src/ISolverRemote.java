import java.sql.SQLException;

import javax.ejb.Remote;
import javax.naming.NamingException;


@Remote
public interface ISolverRemote{
	public double find(String s, double c) 
			throws NamingException, SQLException;
}
