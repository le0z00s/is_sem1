package pl.jrj.game;
import javax.ejb.Remote;

/**
 * NageNonitor ibterface for EJB session bean
 * @author Rafał Zbojak
 */
@Remote
public interface IGameMonitor {
	public boolean register(int hwork, String album); // hwork - numer zadania, album – numer albumu studenta
	public void initGame(String state);
	public String verify(String state); 
}
