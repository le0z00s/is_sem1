import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.ejb.Stateful;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Bean class to calculate the largest field with given C
 * @author Rafał Zbojak
 */
@Stateful
public class Solver implements ISolverRemote{

	private static TreeSet<Point> correct = new TreeSet<Point>();
	private static TreeSet<Point> incorrect = new TreeSet<Point>();

	/**
	 * Finds the field of a plane
	 * @param s Database handler
	 * @param c Color of the plane
	 * @return field containing points with given C
	 */
	@Override
	public double find(String s, double c) 
			throws NamingException, SQLException {
		getData(s, c);
		
		ArrayList<Point> vertexes = graham(correct);
		
		double output = calculateField(vertexes);
		
		return output;
	}

	/**
	 * Retrieves data from database
	 * @param s Database handler
	 * @param c Color of the plane
	 * @throws NamingException
	 * @throws SQLException
	 */
	private static void getData(String s, double c) 
			throws NamingException, SQLException{
		Context ctx = new InitialContext();
		DataSource data = (DataSource) ctx.lookup(s);
		Connection conn = data.getConnection();
		Statement state = conn.createStatement();

		ResultSet result = state.executeQuery("SELECT * FROM Ttable");
		double x;
		double y;
		double z;
		while(result.next()){
			x = result.getDouble(2);
			y = result.getDouble(3);
			z = result.getDouble(4);

			if(z == c){
				correct.add(new Point(x, y));
			}else{
				incorrect.add(new Point(x, y));
			}
		}
	}
	
	/**
	 * Find orientation of given 3 points p1, p2, p3
	 * @param p1 first point of triangle
	 * @param p2 second point of triangle
	 * @param p3 third point of triangle
	 * @return 0 -> p1, p2, p3 colinear, 1 -> cl, 2 -> ccl
	 */
	private static int orientation(Point p1, Point p2, Point p3){
		double value = (p2.getY() - p1.getY()) * (p3.getX() - p2.getX())
				- (p2.getX() - p1.getX()) * (p3.getY() - p2.getY());
		if(value == 0) return 0;
		return (value > 0) ? 1 : 2;
	}
	
	/**
	 * Performs Graham scan on a T plain
	 * @param points Set of sorted entry points
	 * @return List of points creating a closed plain
	 */
	private static ArrayList<Point> graham(TreeSet<Point> points){
		
		if ( points.size() == 0 ) return null;
		
		//build initial stack
		ArrayList<Point> stack = new ArrayList<Point>();		
		ArrayList<Point> pointArray = new ArrayList<Point>(points);
		stack.add(pointArray.get(0));
		stack.add(pointArray.get(1));
		stack.add(pointArray.get(2));
		
		int ptr = 2;
		for (int i = 3 ; i < pointArray.size() ; i++){
			while (orientation(stack.get(ptr), stack.get(ptr-1), 
					pointArray.get(i)) != 2){
				stack.remove(ptr);
				ptr--;
			}
			stack.add(pointArray.get(i));
			ptr++;
		}
		
		return stack;
	}
	
	/**
	 * method returns field of a plain
	 * @param points list of points on a plain
	 * @return field of a smallest possible plain
	 */
	private static double calculateField(ArrayList<Point> points){
		int size = points.size();
		double field = 0;
		
		for ( int i = 2 ; i < size ; i++ ){
			field += triangleField(points.get(0), 
					points.get(i-1), points.get(i));
		}
		
		return field;
	}
	
	/**
	 * Utility method to calculate field of triangle
	 * @param p1 first vertex
	 * @param p2 second vertex
	 * @param p3 third vertex
	 * @return field of triangle
	 */
	private static double triangleField (Point p1, Point p2, Point p3){
		return Math.abs((p1.getX() - p3.getX()) * (p2.getY() - p1.getY()) -
			    ((p1.getX() - p2.getX()) * (p3.getY() - p1.getY())
			    )) / 2;
	}
	
	/**
	 * Utility class representing single point on a plain
	 * @author Rafał Zbojak
	 */
	private static class Point implements Comparable<Point>{
		private final double x;
		private final double y;
		
		/**
		 * Creates point with X Y coordinates
		 * @param x X coordinate of a point
		 * @param y Y coordinate of a point
		 */
		public Point(double x, double y){
			this.x = x;
			this.y = y;
		}
		
		/**
		 * Getter for X coordinate of a point
		 * @return X coordinate of a point
		 */
		public double getX() {
			return x;
		}
		/**
		 * Getter for Y coordinate of a point
		 * @return Y coordinate of a point
		 */
		public double getY() {
			return y;
		}

		@Override
		public int compareTo(Point o) {
			int result = Double.compare(this.getY(), o.getY());
			if(result == 0){
				result = Double.compare(this.getX(), o.getX());
			}
			return result;
		}
	}
}
