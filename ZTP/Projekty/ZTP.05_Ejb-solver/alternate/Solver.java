import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import javax.ejb.Stateful;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Bean class to calculate the largest field with given C
 * @author Rafał Zbojak
 */
@Stateful
public class Solver implements ISolverRemote{

	private static TreeSet<Point> correct = new TreeSet<Point>();
	private static TreeSet<Point> incorrect = new TreeSet<Point>();
	private static ArrayList<Triangle> triangles = new ArrayList<Triangle>();

	/**
	 * Finds the field of a plane
	 * @param s Database handler
	 * @param c Color of the plane
	 * @return field containing points with given C
	 */
	@Override
	public double find(String s, double c) 
			throws NamingException, SQLException {
		getData(s, c);
		getTriangles();
		testTriangles();
		
		double output = 0;
		
		for(Triangle t : triangles){
			output += t.getField();
		}
		
		return output;
	}
	
	/**
	 * Test if any incorrect point is inside of the triangle
	 */
	private static void testTriangles(){
		Iterator<Triangle> it = triangles.iterator();
		while(it.hasNext()){
			for(Point point : incorrect){
				if(checkTriangle(point, it.next())){
					it.remove();
					break;
				}
			}
		}
	}
	
	/**
	 * Get list of all triangles of a plane
	 */
	private static void getTriangles(){
		ArrayList<Point> points = new ArrayList<Point>(correct);
		int aSize = points.size();
		if(aSize>2){
			for(int i = 2 ; i<aSize ; i++){
				Point p1 = points.get(i-2);
				Point p2 = points.get(i-1);
				Point p3 = points.get(i);
				triangles.add(new Triangle(p1, p2, p3));
			}
		}
	}

	/**
	 * Retrieves data from database
	 * @param s Database handler
	 * @param c Color of the plane
	 * @throws NamingException
	 * @throws SQLException
	 */
	private static void getData(String s, double c) 
			throws NamingException, SQLException{
		Context ctx = new InitialContext();
		DataSource data = (DataSource) ctx.lookup(s);
		Connection conn = data.getConnection();
		Statement state = conn.createStatement();

		ResultSet result = state.executeQuery("SELECT * FROM Ttable");
		double x;
		double y;
		double z;
		while(result.next()){
			x = result.getDouble(2);
			y = result.getDouble(3);
			z = result.getDouble(4);

			if(z == c){
				correct.add(new Point(x, y));
			}else{
				incorrect.add(new Point(x, y));
			}
		}
	}
	
	/**
	 * Determines on which side of edge is the point
	 * @param p1 tested point
	 * @param p2 first vertex
	 * @param p3 second vertex
	 * @return
	 */
	private static double sign(Point p1, Point p2, Point p3){
		return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) 
				- (p2.getX() - p3.getX()) * (p1.getY() - p3.getY());
	}
	
	/**
	 * Checks if point is inside of the triangle
	 * @param p point
	 * @param t triangle
	 * @return
	 */
	private static boolean checkTriangle(Point p, Triangle t){
		boolean t1;
		boolean t2;
		boolean t3;
		
		Point v1 = t.getP1();
		Point v2 = t.getP2();
		Point v3 = t.getP3();
		
		t1 = sign(p, v1, v2) < 0.0f;
		t2 = sign(p, v2, v3) < 0.0f;
		t3 = sign(p, v3, v1) < 0.0f;
		
		return ((t1 == t2) && (t2 == t3));
	}

	/**
	 * Utility class representing single point on a plain
	 * @author Rafał Zbojak
	 */
	private static class Point implements Comparable<Point>{
		private final double x;
		private final double y;

		/**
		 * Creates point with X Y coordinates
		 * @param x X coordinate of a point
		 * @param y Y coordinate of a point
		 */
		public Point(double x, double y){
			this.x = x;
			this.y = y;
		}

		/**
		 * Getter for X coordinate of a point
		 * @return X coordinate of a point
		 */
		public double getX() {
			return x;
		}
		/**
		 * Getter for Y coordinate of a point
		 * @return Y coordinate of a point
		 */
		public double getY() {
			return y;
		}

		@Override
		public int compareTo(Point o) {
			int result = Double.compare(this.getY(), o.getY());
			if(result == 0){
				result = Double.compare(this.getX(), o.getX());
			}
			return result;
		}
	}
	
	/**
	 * Utility class representing the triangle
	 */
	private static class Triangle{
		private final Point p1;
		private final Point p2;
		private final Point p3;
		
		/**
		 * Creates triangle with given vertices
		 * @param p1 first vertex
		 * @param p2 second vertex
		 * @param p3 third vertex
		 */
		public Triangle(Point p1, Point p2, Point p3) {
			this.p1 = p1;
			this.p2 = p2;
			this.p3 = p3;			
		}

		/**
		 * Getter for the 1st vertex
		 * @return vertex
		 */
		public Point getP1() {
			return p1;
		}

		/**
		 * Getter for the 2nd vertex
		 * @return vertex
		 */
		public Point getP2() {
			return p2;
		}

		/**
		 * Getter for the 3rd vertex
		 * @return vertex
		 */
		public Point getP3() {
			return p3;
		}
		
		/**
		 * Calculates field of a triangle
		 * @return field
		 */
		public double getField(){
			return Math.abs((p1.getX() - p3.getX()) * (p2.getY() - p1.getY()) 
					- ((p1.getX() - p2.getX()) * (p3.getY() - p1.getY())))/ 2;
		}
	}
}
