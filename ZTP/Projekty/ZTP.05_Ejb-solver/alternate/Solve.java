import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.jrj.game.IGameMonitor;


public class Solve extends HttpServlet {
	private static final long serialVersionUID = 1223955438922310753L;

	@EJB(mappedName = "java:global/ejb-project/GameMonitor")
	private IGameMonitor monitor;
	
	@EJB
	private ISolverRemote solver;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		String s = req.getParameter("s");
		Double c = Double.parseDouble(req.getParameter("c"));
		PrintWriter output = res.getWriter();
		
		if(monitor.register(5, "93514")){
			double field;
			try {
				field = solver.find(s, c);
			} catch (NamingException | SQLException e) {
				field = 0;
			}
			output.format("%.5f", field);
		}
	}
}
