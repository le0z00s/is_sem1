package pl.jrj.data;

import javax.ejb.Remote;

/**
 * Interface implemented in one of the statefull beans 
 * deployed on the application server
 * @author Rafał Zbojak
 */
@Remote
public interface IDataMonitor {
	public boolean register(int hwork, String album);
	public boolean hasNext();
	public double next();
}
