package pl.jrj.data;

import java.util.ArrayList;
import java.util.TreeSet;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * CLient class. It searches appropriate statefull bean
 * in the EJB container, then executes register method and gets
 * array of (x, y) points. After that it calculates the biggest
 * possible field of a figure defined by the provided data and returns
 * answer rounded to the fifth decimal place.
 * @author Rafał Zbojak
 */
public class Appclient {

	
	/**
	 * Application's main method.
	 * @param args Command line arguments
	 */
	public static void main(String[] args){
		String beanString = "java:global/ejb-project/DataMonitor!pl.jrj.data.IDataMonitor";
		try {
			IDataMonitor monitor = (IDataMonitor) new InitialContext()
				.lookup(beanString);
			
			TreeSet<Point> points = new TreeSet<Point>();
			
			if(monitor.register(4, "93514")) {
				int elCount = 0;
				double x = 0;
				double y = 0;
				
				while(monitor.hasNext()) {
					elCount++;
					if(elCount%2 != 0) {
						x = monitor.next();
					}else {
						y = monitor.next();
						points.add(new Point(x, y));
					}
				}
			}
			
			ArrayList<Point> vertexes = graham(points);
			double field = calculateField(vertexes);
			System.out.format("%.5f", field);
		} catch (NamingException e) {
			e.printStackTrace();
		}			
	}

	/**
	 * Find orientation of given 3 points p1, p2, p3
	 * @param p1 first point of triangle
	 * @param p2 second point of triangle
	 * @param p3 third point of triangle
	 * @return 0 -> p1, p2, p3 colinear, 1 -> cl, 2 -> ccl
	 */
	private static int orientation(Point p1, Point p2, Point p3){
		double value = (p2.getY() - p1.getY()) * (p3.getX() - p2.getX())
				- (p2.getX() - p1.getX()) * (p3.getY() - p2.getY());
		if(value == 0) return 0;
		return (value > 0) ? 1 : 2;
	}
	
	/**
	 * Performs Graham scan on a T plain
	 * @param points Set of sorted entry points
	 * @return List of points creating a closed plain
	 */
	private static ArrayList<Point> graham(TreeSet<Point> points){
		
		if ( points.size() == 0 ) return null;
		
		//build initial stack
		ArrayList<Point> stack = new ArrayList<Point>();		
		ArrayList<Point> pointArray = new ArrayList<Point>(points);
		stack.add(pointArray.get(0));
		stack.add(pointArray.get(1));
		stack.add(pointArray.get(2));
		
		int ptr = 2;
		for (int i = 3 ; i < pointArray.size() ; i++){
			while (orientation(stack.get(ptr), stack.get(ptr-1), pointArray.get(i)) != 2){
				stack.remove(ptr);
				ptr--;
			}
			stack.add(pointArray.get(i));
			ptr++;
		}
		
		return stack;
	}
	
	/**
	 * method returns field of a plain
	 * @param points list of points on a plain
	 * @return field of a smallest possible plain
	 */
	public static double calculateField(ArrayList<Point> points){
		int size = points.size();
		double field = 0;
		
		for ( int i = 2 ; i < size ; i++ ){
			field += triangleField(points.get(0), points.get(i-1), points.get(i));
		}
		
		return field;
	}
	
	/**
	 * Utility method to calculate field of triangle
	 * @param p1 first vertex
	 * @param p2 second vertex
	 * @param p3 third vertex
	 * @return field of triangle
	 */
	private static double triangleField (Point p1, Point p2, Point p3){
		return Math.abs((p1.getX() - p3.getX()) * (p2.getY() - p1.getY()) -
			    ((p1.getX() - p2.getX()) * (p3.getY() - p1.getY())
			    )) / 2;
	}
	
	/**
	 * Utility class representing single point on a plain
	 * @author Rafał Zbojak
	 */
	public static class Point implements Comparable<Point>{
		private final double x;
		private final double y;
		
		/**
		 * Creates point with X Y coordinates
		 * @param x X coordinate of a point
		 * @param y Y coordinate of a point
		 */
		public Point(double x, double y){
			this.x = x;
			this.y = y;
		}
		
		/**
		 * Getter for X coordinate of a point
		 * @return X coordinate of a point
		 */
		public double getX() {
			return x;
		}
		/**
		 * Getter for Y coordinate of a point
		 * @return Y coordinate of a point
		 */
		public double getY() {
			return y;
		}

		@Override
		public int compareTo(Point o) {
			int result = Double.compare(this.getY(), o.getY());
			if(result == 0){
				result = Double.compare(this.getX(), o.getX());
			}
			return result;
		}
	}
}
