package lab;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @EJB 
    TriangleBean bean;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");   
        response.setCharacterEncoding("UTF-8");            
        PrintWriter out = response.getWriter();
        
        double a = Double.parseDouble(request.getParameter("edgea"));
        double b = Double.parseDouble(request.getParameter("edgeb"));
        double c = Double.parseDouble(request.getParameter("edgec"));
        
        if(!bean.testTriangle(a, b, c)){
        	out.println("Cannot create triangle with given edges");
        }else {
			out.format(Locale.US, "Triangle field: %.2f", bean.calculateField(a, b, c));
		}
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

    public MainServlet(){
    	this.bean = new TriangleBean();
    }
}
