package lab;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class TriangleBean {

	public TriangleBean(){}

	public boolean testTriangle(double a, double b, double c){
		if((a + b > c) && (a + c > b) && (b + c > a)){
			return true;
		}
		return false;
	}

	public double calculateField(double a, double b, double c){
		double field = 0f;
		if(testTriangle(a, b, c)){
			double p = (a+b+c)/2;

			field = Math.sqrt(p*(p-a)*(p-b)*(p-c));
		}
		return field;
	}
}
