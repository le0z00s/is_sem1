/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "autorksiazka")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Autorksiazka.findAll", query = "SELECT a FROM Autorksiazka a"),
    @NamedQuery(name = "Autorksiazka.findById", query = "SELECT a FROM Autorksiazka a WHERE a.id = :id")})
public class Autorksiazka implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "AutorID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Autor autorID;
    @JoinColumn(name = "KsiazkaID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ksiazka ksiazkaID;

    public Autorksiazka() {
    }

    public Autorksiazka(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Autor getAutorID() {
        return autorID;
    }

    public void setAutorID(Autor autorID) {
        this.autorID = autorID;
    }

    public Ksiazka getKsiazkaID() {
        return ksiazkaID;
    }

    public void setKsiazkaID(Ksiazka ksiazkaID) {
        this.ksiazkaID = ksiazkaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autorksiazka)) {
            return false;
        }
        Autorksiazka other = (Autorksiazka) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.solek.Autorksiazka[ id=" + id + " ]";
    }
    
}
