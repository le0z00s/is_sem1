/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "wypozyczenie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wypozyczenie.findAll", query = "SELECT w FROM Wypozyczenie w"),
    @NamedQuery(name = "Wypozyczenie.findById", query = "SELECT w FROM Wypozyczenie w WHERE w.id = :id"),
    @NamedQuery(name = "Wypozyczenie.findByDataWypozyczenia", query = "SELECT w FROM Wypozyczenie w WHERE w.dataWypozyczenia = :dataWypozyczenia"),
    @NamedQuery(name = "Wypozyczenie.findByPlanowanaDataOddania", query = "SELECT w FROM Wypozyczenie w WHERE w.planowanaDataOddania = :planowanaDataOddania"),
    @NamedQuery(name = "Wypozyczenie.findByDataOddania", query = "SELECT w FROM Wypozyczenie w WHERE w.dataOddania = :dataOddania")})
public class Wypozyczenie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DataWypozyczenia")
    @Temporal(TemporalType.DATE)
    private Date dataWypozyczenia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PlanowanaDataOddania")
    @Temporal(TemporalType.DATE)
    private Date planowanaDataOddania;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DataOddania")
    @Temporal(TemporalType.DATE)
    private Date dataOddania;
    @JoinColumn(name = "KsiazkaID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ksiazka ksiazkaID;
    @JoinColumn(name = "UzytkownikID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Uzytkownik uzytkownikID;

    public Wypozyczenie() {
    }

    public Wypozyczenie(Integer id) {
        this.id = id;
    }

    public Wypozyczenie(Integer id, Date dataWypozyczenia, Date planowanaDataOddania, Date dataOddania) {
        this.id = id;
        this.dataWypozyczenia = dataWypozyczenia;
        this.planowanaDataOddania = planowanaDataOddania;
        this.dataOddania = dataOddania;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataWypozyczenia() {
        return dataWypozyczenia;
    }

    public void setDataWypozyczenia(Date dataWypozyczenia) {
        this.dataWypozyczenia = dataWypozyczenia;
    }

    public Date getPlanowanaDataOddania() {
        return planowanaDataOddania;
    }

    public void setPlanowanaDataOddania(Date planowanaDataOddania) {
        this.planowanaDataOddania = planowanaDataOddania;
    }

    public Date getDataOddania() {
        return dataOddania;
    }

    public void setDataOddania(Date dataOddania) {
        this.dataOddania = dataOddania;
    }

    public Ksiazka getKsiazkaID() {
        return ksiazkaID;
    }

    public void setKsiazkaID(Ksiazka ksiazkaID) {
        this.ksiazkaID = ksiazkaID;
    }

    public Uzytkownik getUzytkownikID() {
        return uzytkownikID;
    }

    public void setUzytkownikID(Uzytkownik uzytkownikID) {
        this.uzytkownikID = uzytkownikID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wypozyczenie)) {
            return false;
        }
        Wypozyczenie other = (Wypozyczenie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.solek.Wypozyczenie[ id=" + id + " ]";
    }
    
}
