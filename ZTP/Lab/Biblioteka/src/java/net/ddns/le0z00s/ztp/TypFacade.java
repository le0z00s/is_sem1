/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Rafał Zbojak
 */
@Stateless
public class TypFacade extends AbstractFacade<Typ> {

    @PersistenceContext(unitName = "BibliotekaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TypFacade() {
        super(Typ.class);
    }
    
}
