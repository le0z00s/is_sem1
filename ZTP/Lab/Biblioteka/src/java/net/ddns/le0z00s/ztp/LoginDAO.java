/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rafał Zbojak
 */
public class LoginDAO {
        public static int validate(String user, String password) {
        Connection con = null;
        PreparedStatement ps = null;
 
        try {
            con = getConnection();
            ps = con.prepareStatement("Select TypID from Uzytkownik where Email = ? and Haslo = ?");
            ps.setString(1, user);
            ps.setString(2, password);
 
            ResultSet rs = ps.executeQuery();
            int typId = 0;
            if (rs.next()) {
                typId = rs.getInt("TypID");
            }
            return typId;
        } catch (SQLException ex) {
            System.out.println("Login error -->" + ex.getMessage());
             return 0;
        } finally {
            close(con);
        }
    }
        
        
    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/biblioteka", "root", "mysql");
            return con;
        } catch (Exception ex) {
            System.out.println("Database.getConnection() Error -->"
                    + ex.getMessage());
            return null;
        }
    }
 
    public static void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
}
