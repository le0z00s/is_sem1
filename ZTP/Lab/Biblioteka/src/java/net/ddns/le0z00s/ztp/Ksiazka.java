/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "ksiazka")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ksiazka.findAll", query = "SELECT k FROM Ksiazka k"),
    @NamedQuery(name = "Ksiazka.findById", query = "SELECT k FROM Ksiazka k WHERE k.id = :id"),
    @NamedQuery(name = "Ksiazka.findByIsbn", query = "SELECT k FROM Ksiazka k WHERE k.isbn = :isbn"),
    @NamedQuery(name = "Ksiazka.findByTytul", query = "SELECT k FROM Ksiazka k WHERE k.tytul = :tytul"),
    @NamedQuery(name = "Ksiazka.findByEdycja", query = "SELECT k FROM Ksiazka k WHERE k.edycja = :edycja"),
    @NamedQuery(name = "Ksiazka.findByRok", query = "SELECT k FROM Ksiazka k WHERE k.rok = :rok"),
    @NamedQuery(name = "Ksiazka.findByStrony", query = "SELECT k FROM Ksiazka k WHERE k.strony = :strony"),
    @NamedQuery(name = "Ksiazka.findByOpis", query = "SELECT k FROM Ksiazka k WHERE k.opis = :opis")})
public class Ksiazka implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ISBN")
    private String isbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "Tytul")
    private String tytul;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Edycja")
    private String edycja;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Rok")
    private int rok;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Strony")
    private int strony;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "Opis")
    private String opis;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ksiazkaID")
    private Collection<Autorksiazka> autorksiazkaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ksiazkaID")
    private Collection<Rezerwacja> rezerwacjaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ksiazkaID")
    private Collection<Wypozyczenie> wypozyczenieCollection;

    public Ksiazka() {
    }

    public Ksiazka(Integer id) {
        this.id = id;
    }

    public Ksiazka(Integer id, String isbn, String tytul, String edycja, int rok, int strony, String opis) {
        this.id = id;
        this.isbn = isbn;
        this.tytul = tytul;
        this.edycja = edycja;
        this.rok = rok;
        this.strony = strony;
        this.opis = opis;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getEdycja() {
        return edycja;
    }

    public void setEdycja(String edycja) {
        this.edycja = edycja;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public int getStrony() {
        return strony;
    }

    public void setStrony(int strony) {
        this.strony = strony;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @XmlTransient
    public Collection<Autorksiazka> getAutorksiazkaCollection() {
        return autorksiazkaCollection;
    }

    public void setAutorksiazkaCollection(Collection<Autorksiazka> autorksiazkaCollection) {
        this.autorksiazkaCollection = autorksiazkaCollection;
    }

    @XmlTransient
    public Collection<Rezerwacja> getRezerwacjaCollection() {
        return rezerwacjaCollection;
    }

    public void setRezerwacjaCollection(Collection<Rezerwacja> rezerwacjaCollection) {
        this.rezerwacjaCollection = rezerwacjaCollection;
    }

    @XmlTransient
    public Collection<Wypozyczenie> getWypozyczenieCollection() {
        return wypozyczenieCollection;
    }

    public void setWypozyczenieCollection(Collection<Wypozyczenie> wypozyczenieCollection) {
        this.wypozyczenieCollection = wypozyczenieCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ksiazka)) {
            return false;
        }
        Ksiazka other = (Ksiazka) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tytul;
    }
    
}
