/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rafał Zbojak
 */
@Entity
@Table(name = "rezerwacja")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rezerwacja.findAll", query = "SELECT r FROM Rezerwacja r"),
    @NamedQuery(name = "Rezerwacja.findById", query = "SELECT r FROM Rezerwacja r WHERE r.id = :id"),
    @NamedQuery(name = "Rezerwacja.findByDataRezerwacji", query = "SELECT r FROM Rezerwacja r WHERE r.dataRezerwacji = :dataRezerwacji"),
    @NamedQuery(name = "Rezerwacja.findByKoniecRezerwacji", query = "SELECT r FROM Rezerwacja r WHERE r.koniecRezerwacji = :koniecRezerwacji")})
public class Rezerwacja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DataRezerwacji")
    @Temporal(TemporalType.DATE)
    private Date dataRezerwacji;
    @Basic(optional = false)
    @NotNull
    @Column(name = "KoniecRezerwacji")
    @Temporal(TemporalType.DATE)
    private Date koniecRezerwacji;
    @JoinColumn(name = "KsiazkaID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ksiazka ksiazkaID;
    @JoinColumn(name = "UzytkownikID", referencedColumnName = "ID")
     @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Uzytkownik uzytkownikID;

    public Rezerwacja() {
    }

    public Rezerwacja(Integer id) {
        this.id = id;
    }

    public Rezerwacja(Integer id, Date dataRezerwacji, Date koniecRezerwacji) {
        this.id = id;
        this.dataRezerwacji = dataRezerwacji;
        this.koniecRezerwacji = koniecRezerwacji;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataRezerwacji() {
        return dataRezerwacji;
    }

    public void setDataRezerwacji(Date dataRezerwacji) {
        this.dataRezerwacji = dataRezerwacji;
    }

    public Date getKoniecRezerwacji() {
        return koniecRezerwacji;
    }

    public void setKoniecRezerwacji(Date koniecRezerwacji) {
        this.koniecRezerwacji = koniecRezerwacji;
    }

    public Ksiazka getKsiazkaID() {
        return ksiazkaID;
    }

    public void setKsiazkaID(Ksiazka ksiazkaID) {
        this.ksiazkaID = ksiazkaID;
    }

    public Uzytkownik getUzytkownikID() {
        return uzytkownikID;
    }

    public void setUzytkownikID(Uzytkownik uzytkownikID) {
        this.uzytkownikID = uzytkownikID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rezerwacja)) {
            return false;
        }
        Rezerwacja other = (Rezerwacja) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rezerwacja od:" + dataRezerwacji + " do: " + koniecRezerwacji;
    }
    
}
