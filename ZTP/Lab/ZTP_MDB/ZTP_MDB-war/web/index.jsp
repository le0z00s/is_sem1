<%-- 
    Document   : index
    Created on : 2016-03-18, 19:36:04
    Author     : Rafał Zbojak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ZTP - MDB</title>
    </head>
    <body>
        <form action="NewServlet">
            <table border="1">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Wyślij wiadomość:</td>
                        <td><input type="text" name="msg" value="" size="50" /></td>
                    </tr>
                </tbody>
            </table>
            <input type="submit" value="Wyślij" />
        </form>
    </body>
</html>
