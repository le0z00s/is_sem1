/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.ddns.le0z00s.ztp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rafał Zbojak
 */
@WebServlet(name = "ConcurrentServlet", urlPatterns = {"/ConcurrentServlet"})
public class ConcurrentServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2763354357664433746L;
	private String lastName;
    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    
    public void init(){
        lastName = "NoName";
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html");         
         PrintWriter out = response.getWriter();         
         out.println("<html>");         
         out.println("<body>");         
         out.println("name: <b>" + this.lastName + "</b>");         
         out.println("</body>");         
         out.println("</html>");         
         out.close();        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String paramName = new String(request.getParameter("last_name").getBytes("iso-8859-1"),"UTF-8");         
         this.lastName = paramName;                  
         response.sendRedirect("ConcurrentServlet"); 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
