package ztbd.project.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

public class Settings {
	private static String propPath = "settings.ini";
	
	public String getProperty(String key, String defaultValue){
		Properties prop = new Properties();
		Path path = Paths.get(propPath);
		InputStream input = null;
		try{
			input = Files.newInputStream(path, StandardOpenOption.CREATE);
			prop.load(input);
			input.close();
		} catch (IOException e) {
			//store value - mostly redundant, but I'm lazy
			OutputStream out;
			try {
				out = Files.newOutputStream(path);
				prop.setProperty(key, defaultValue);
				prop.store(out, null);
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return prop.getProperty(key, defaultValue);
	}
}
