package ztbd.project.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.AbstractTableModel;

import ztbd.project.database.Connect;
import ztbd.project.properties.Settings;

public class MainWindow implements Runnable {

    private Connect connect;
    private JFrame frame;
    private static String title;

    public MainWindow(Connect connect) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        this.connect = connect;
        Settings settings = new Settings();
        title = settings.getProperty("title", "Bazy danych przechowywane w pamieci");

        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    createAndShowGUI();
                }
            });
        }
    }

    public void run() {

    }

    private JFrame createAndShowGUI() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentToPane(frame.getContentPane());
        frame.pack();
        frame.setLocation(50, 50);
        frame.setSize(800, 600);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setTitle(title);
        return frame;
    }

    private void addComponentToPane(Container pane) {
        JPanel logoPane = getLogo();

        JTabbedPane tabbedPane = new JTabbedPane();

        JPanel readingsPane = new ReadingsPanel("View Readings");
        JPanel devicesPane = new DevicesPanel("View Devices");

        tabbedPane.addTab("Sensor readings", readingsPane);
        tabbedPane.addTab("List of devices", devicesPane);

        pane.add(logoPane, BorderLayout.NORTH);
        pane.add(tabbedPane, BorderLayout.SOUTH);
    }

    private JPanel getLogo() {
        final ImageIcon logo = new ImageIcon(ClassLoader.getSystemResource( "logo.png" ));
        JPanel logoPane = new JPanel() {
            private static final long serialVersionUID = 1L;

            protected void paintComponent(Graphics g) {
                Dimension d = getSize();
                g.drawImage(logo.getImage(), 0, 0, d.width, d.height, null);
                super.paintComponent(g);
            }
        };
        logoPane.setOpaque(false);
        logoPane.setPreferredSize(new Dimension(logo.getIconWidth(), logo.getIconHeight()));
        return logoPane;
    }

    private class ReadingsPanel extends JPanel {

        private static final long serialVersionUID = -6725405660923724733L;
        @SuppressWarnings("unused")
        private String panelName;

        public ReadingsPanel(String panelName) {
            this.panelName = panelName;
            this.setLayout(new BorderLayout());
            addComponentListener(new ComponentAdapter() {
                @Override
                public void componentHidden(ComponentEvent evt) {

                }

                @Override
                public void componentShown(ComponentEvent evt) {
                    ResultSet resultSet;
                    QueryTableModel queryTableModel = new QueryTableModel();
                    try {
                        resultSet = connect.selectReadings();
                        queryTableModel = new QueryTableModel(resultSet);
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    JTable table = new JTable(queryTableModel);
                    JScrollPane pane = new JScrollPane(table);
                    add(pane);
                    revalidate();
                }
            });

        }
    }

    private class DevicesPanel extends JPanel {

        private static final long serialVersionUID = -3330676358033793357L;
        @SuppressWarnings("unused")
        private String panelName = "View Devices";

        public DevicesPanel(String panelName) {
            this.panelName = panelName;
            this.setLayout(new BorderLayout());
            addComponentListener(new ComponentAdapter() {
                @Override
                public void componentHidden(ComponentEvent evt) {
                	
                }

                @Override
                public void componentShown(ComponentEvent evt) {
                    ResultSet resultSet;
                    QueryTableModel queryTableModel = new QueryTableModel();
                    try {
                        resultSet = connect.selectDevices();
                        queryTableModel = new QueryTableModel(resultSet);
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    JTable table = new JTable(queryTableModel);
                    JScrollPane pane = new JScrollPane(table);
                    add(pane);
                    revalidate();
                }
            });
        }
    }

    private class QueryTableModel extends AbstractTableModel {
		private static final long serialVersionUID = -3180895153922346037L;

		Vector<String[]> cache; // will hold String[] objects . . .

        int colCount;

        String[] headers;

        public QueryTableModel() {
            cache = new Vector<String[]>();
        }

        public QueryTableModel(ResultSet resultSet) throws SQLException {
            cache = new Vector<String[]>();
            ResultSetMetaData meta = resultSet.getMetaData();
            colCount = meta.getColumnCount();

            // Now we must rebuild the headers array with the new column names
            headers = new String[colCount];
            for (int h = 1; h <= colCount; h++) {
                headers[h - 1] = meta.getColumnName(h);
            }

            // and file the cache with the records from our query. This would
            // not be
            // practical if we were expecting a few million records in response
            // to our
            // query, but we aren't, so we can do this.
            while (resultSet.next()) {
                String[] record = new String[colCount];
                for (int i = 0; i < colCount; i++) {
                    record[i] = resultSet.getString(i + 1);
                }
                cache.addElement(record);
            }
            fireTableChanged(null);
        }

        public String getColumnName(int i) {
            return headers[i];
        }

        public int getColumnCount() {
            return colCount;
        }

        public int getRowCount() {
            return cache.size();
        }

        public Object getValueAt(int row, int col) {
            return ((String[]) cache.elementAt(row))[col];
        }
    }
}
