package ztbd.project.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.tools.Server;

import ztbd.project.bean.Config;
import ztbd.project.bean.Efento;
import ztbd.project.properties.Settings;

public class Connect {

    private Server server;
    private Connection conn;
    boolean debug;

    public Connect() {
        Settings settings = new Settings();
        debug = Boolean.parseBoolean(
                settings.getProperty("debug", "true"));
    }

    private static final String DB_DRIVER = "org.h2.Driver";

    private void init() throws SQLException {
        String[] args = new String[]{
            "-tcpPort", "8092"};
        server = Server.createTcpServer(args).start();
    }

    public void closeServer() throws SQLException {
        server.stop();
        conn.close();
    }

    public void connect(String path) throws Exception {
        Class.forName(DB_DRIVER);
        conn = DriverManager.getConnection("jdbc:h2:" + path, "SA", "");
        createSchema();
        conn.commit();
        init();

    }

    public Connection getConnection() {
        return conn;
    }

    public void createSchema() throws SQLException {
        long time = System.currentTimeMillis();
        conn.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS PUBLIC.device("
                + "device_id varchar(16) NOT NULL,"
                + "name varchar(255) DEFAULT NULL,"
                + "description varchar(255) DEFAULT NULL,"
                + "PRIMARY KEY (device_id)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );
        conn.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS PUBLIC.device_config("
                + "id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,"
                + "device_id varchar(16) NOT NULL,"
                + "sensor_low float DEFAULT NULL,"
                + "sensor_high float DEFAULT NULL,"
                + "schedule int(10) NOT NULL,"
                + "PRIMARY KEY (id)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

        conn.createStatement().execute(
                "CREATE INDEX device_config_device_FK1 ON PUBLIC.device_config(device_id ASC);"
        );

        conn.createStatement().execute(
                "CREATE TABLE IF NOT EXISTS PUBLIC.device_reading("
                + "id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,"
                + "device_id varchar(16) NOT NULL,"
                + "reading_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
                + "sensor_reading float DEFAULT NULL,"
                + "battery_level int(8) DEFAULT NULL,"
                + "signal int(3) DEFAULT NULL,"
                + "PRIMARY KEY (id)"
                + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

        conn.createStatement().execute(
                "CREATE INDEX device_reading_device_FK1 ON PUBLIC.device_reading(device_id ASC);"
        );

        conn.createStatement().execute(
                "ALTER TABLE PUBLIC.device_config "
                + "ADD FOREIGN KEY (device_id) "
                + "REFERENCES PUBLIC.device(device_id) "
                + "ON UPDATE CASCADE "
                + "ON DELETE CASCADE;"
        );

        conn.createStatement().execute(
                "ALTER TABLE PUBLIC.device_reading "
                + "ADD FOREIGN KEY (device_id) "
                + "REFERENCES PUBLIC.device(device_id) "
                + "ON UPDATE CASCADE "
                + "ON DELETE CASCADE;"
        );
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Created schema in %d ms.\n", time);
        }
    }

    public void insertReading(Efento efento) throws SQLException {
        //conn.setAutoCommit(false);
        String query = "REPLACE INTO PUBLIC.device (device_id, name, description) VALUES (?, ?, ?)";
        PreparedStatement prep = conn.prepareStatement(query);
        prep.setString(1, efento.getSerial());
        prep.setString(2, efento.getName());
        prep.setString(3, efento.getName() + efento.getVersion());
        prep.addBatch();
        prep.executeBatch();

        query = "INSERT INTO PUBLIC.device_reading (device_id, sensor_reading, battery_level, signal) VALUES (?, ?, ?, ?)";
        prep = conn.prepareStatement(query);
        prep.setString(1, efento.getSerial());
        prep.setDouble(2, efento.getTemperature());
        prep.setInt(3, efento.getBatLevel());
        prep.setInt(4, efento.getSignalStrength());
        prep.addBatch();
        prep.executeBatch();

        long time = System.currentTimeMillis();
        conn.commit();
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Inserted new reading in %d ms.\n", time);
        }
        conn.setAutoCommit(true);
    }

    public void insertConfig(Config config) throws SQLException {
        conn.setAutoCommit(false);
        String query = "REPLACE INTO PUBLIC.device_config (device_id, sensor_low, sensor_high, schedule) VALUES (?, ?, ?, ?)";
        PreparedStatement prep = conn.prepareStatement(query);
        prep.setString(1, config.getDevice_id());
        prep.setDouble(2, config.getSensor_low());
        prep.setDouble(3, config.getSensor_high());
        prep.setInt(4, config.getSchedule());
        prep.addBatch();

        long time = System.currentTimeMillis();
        prep.executeBatch();
        conn.commit();
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Inserted new config in %d ms.\n", time);
        }
        conn.setAutoCommit(true);
    }

    public ResultSet selectReadings() throws SQLException {
        String query = "SELECT * FROM PUBLIC.device_reading";

        Statement statement = conn.createStatement();
        long time = System.currentTimeMillis();
        ResultSet resultSet = statement.executeQuery(query);
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Selected all readings in %d ms.\n", time);
        }
        return resultSet;
    }

    public ResultSet selectReadings(String device_id) throws SQLException {
        String query = String.format("SELECT * FROM PUBLIC.device_reading WHERE device_id=%s", device_id);

        Statement statement = conn.createStatement();
        long time = System.currentTimeMillis();
        ResultSet resultSet = statement.executeQuery(query);
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Selected readings for %s in %d ms.\n", device_id, time);
        }
        return resultSet;
    }

    public ResultSet selectConfigs() throws SQLException {
        String query = "SELECT * FROM PUBLIC.device_config";

        Statement statement = conn.createStatement();
        long time = System.currentTimeMillis();
        ResultSet resultSet = statement.executeQuery(query);
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Selected all configs in %d ms.\n", time);
        }
        return resultSet;
    }

    public ResultSet selectDevices() throws SQLException {
        String query = "SELECT * FROM PUBLIC.device";

        Statement statement = conn.createStatement();
        long time = System.currentTimeMillis();
        ResultSet resultSet = statement.executeQuery(query);
        time = System.currentTimeMillis() - time;
        if (debug) {
            System.out.format("Selected all devices in %d ms.\n", time);
        }
        return resultSet;
    }
}
