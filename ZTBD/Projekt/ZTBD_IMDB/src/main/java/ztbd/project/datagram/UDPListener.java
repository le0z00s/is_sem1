package ztbd.project.datagram;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import ztbd.project.bean.Efento;
import ztbd.project.database.Connect;
import ztbd.project.packet.Packet;
import ztbd.project.properties.Settings;
import ztbd.project.util.Utils;

public class UDPListener implements Runnable{

	private int UDP_PORT;
	private boolean debug;
	private Connect connect;

	public UDPListener(Connect connect){
		Settings settings = new Settings();
		int port = Integer.parseInt(
				settings.getProperty("udp_port", "20000"));
		boolean debug = Boolean.parseBoolean(
				settings.getProperty("debug", "true"));
		
		this.connect = connect;
		this.UDP_PORT = port;
		this.debug = debug;
	}

	public void run() {		
		DatagramSocket socket = null;
		DatagramPacket receivePacket = null;
		boolean stopped = false;

		try {
			socket = new DatagramSocket(UDP_PORT);
			byte[] receiveData = new byte[172];//Packet size is 174 bytes including new line and caret return. The last 2 bytes are stripped, because they're not needed.
			System.out.println("Starting listening on udp:"+InetAddress.getLocalHost().getHostAddress()+":"+UDP_PORT);
			receivePacket = new DatagramPacket(receiveData, receiveData.length);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, Integer> frames = new HashMap<String, Integer>();//store sensor frame number

		while(!stopped){
			try {
				socket.receive(receivePacket);

				//get whole packet
				String data = new String(receivePacket.getData(), 0, receivePacket.getLength());
				//System.out.println("Received: "+data);

				//parse it to byte array
				byte[] parsedPacket = Utils.hexStringToByteArray(data);

				Packet packet = new Packet(parsedPacket);
				if(Utils.isEfento(packet)){

					Efento efento = new Efento(packet.getBleAddress(), packet.getBleName(), packet.getSignalStrength(), packet.getBleData());

					if(frames!=null && frames.containsKey(efento.getSerial())){
						if(frames.get( efento.getSerial() ) != efento.getCounter()){
							frames.put(efento.getSerial(), efento.getCounter());	

							connect.insertReading(efento);

							if(debug){
								printPacketData(packet);
								printEfentoData(packet.getGatewayAddress(),efento);
							}
						}
					}else{
						frames.put(efento.getSerial(), efento.getCounter());

						connect.insertReading(efento);

						if(debug){
							printPacketData(packet);
							printEfentoData(packet.getGatewayAddress(),efento);
						}
					}

				}
			} catch (IOException e) {
				stopped = true;
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void printPacketData(Packet packet){
		//get sensor data from the packet. not needed and will be cut away after debugging
		String bufferData = "";
		if(packet.getBleData()!=null){
			for(byte b : packet.getBleData()){
				bufferData +=Utils.byteToString(b);
			}
		}
		System.out.println("Data: "+bufferData);
	}

	private void printEfentoData(String gatewayAddress, Efento efento){
		System.out.println();

		//cut gateway MAC from the packet
		System.out.println("Gateway MAC: "+gatewayAddress);

		System.out.println("Serial: "+efento.getSerial());
		System.out.println("Signal Strength: "+efento.getSignalStrength()+"dBm");
		System.out.println("Name: "+efento.getName());
		System.out.println("Packet: "+efento.getCounter());
		System.out.println("Temperature: "+efento.getTemperature()+"C");
		System.out.println("Period: "+efento.getPeriod());
		System.out.println("Version: "+efento.getVersion());
		System.out.println("Battery: "+efento.getBatLevel()+"mV");

		System.out.println();
	}
}
