package ztbd.project;

import java.sql.SQLException;
import java.util.Random;

import javax.swing.UnsupportedLookAndFeelException;

import ztbd.project.bean.Efento;
import ztbd.project.database.Connect;
import ztbd.project.datagram.UDPListener;
import ztbd.project.gui.MainWindow;
import ztbd.project.properties.Settings;
import ztbd.project.util.Utils;

public class Main {

    private static String dbPath;

    public Main() {

    }

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, SQLException {

        Settings settings = new Settings();
        dbPath = settings.getProperty("dbPath", "mem:db1;DB_CLOSE_DELAY=-1");
        boolean debug = Boolean.parseBoolean(settings.getProperty("debug", "true"));
        
        Connect connect = new Connect();

        try {
            connect.connect(dbPath);
            long time = System.currentTimeMillis();
            //load dummy data
            Random random = new Random();
            int id;
            byte signal;
            int i;
            for(i=0; i<100000; i++){
            	id = random.nextInt();
            	signal = (byte)random.nextInt();
            	try {
					connect.insertReading(new Efento(""+id , ""+signal, signal, Utils.hexStringToByteArray("ff6c0201a6ffffb688030001c6")));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
            time = System.currentTimeMillis() - time;
            if (debug) {
                System.out.format("Inserted %d rows in %d ms.\n", i, time);
            }
            
            UDPListener listener = new UDPListener(connect);
            MainWindow window = new MainWindow(connect);
            Thread listenerThread = new Thread(listener);
            Thread guiThread = new Thread(window);
            listenerThread.start();
            guiThread.start();

        } catch (Exception e) {
            connect.closeServer();
            e.printStackTrace();
        }
    }
}
