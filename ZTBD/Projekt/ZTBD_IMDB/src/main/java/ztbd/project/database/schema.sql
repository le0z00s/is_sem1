CREATE TABLE IF NOT EXISTS device(
device_id varchar(16) NOT NULL,
name varchar(255) DEFAULT NULL,
description varchar(255) DEFAULT NULL,
PRIMARY KEY (device_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS device_config(
id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
device_id varchar(16) NOT NULL,
sensor_low float DEFAULT NULL,
sensor_high float DEFAULT NULL,
schedule int(10) NOT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX device_config_device_FK1 ON device_config(device_id ASC);

CREATE TABLE IF NOT EXISTS device_reading(
id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
device_id varchar(16) NOT NULL,
reading_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
sensor_reading float DEFAULT NULL,
battery_level int(8) DEFAULT NULL,
signal int(3) DEFAULT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX device_reading_device_FK1 ON device_reading(device_id ASC);

ALTER TABLE device_config 
ADD FOREIGN KEY (device_id) 
REFERENCES device(device_id) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE device_reading 
ADD FOREIGN KEY (device_id) 
REFERENCES device(device_id) 
ON UPDATE CASCADE
ON DELETE CASCADE;
