package ztbd.project.packet;

import java.util.Arrays;

import ztbd.project.util.Utils;

public class Packet {
	private int packetNumber;
	private String bleAddress;
	private byte signalStrength;
	private byte[] broadcast;
	private String gatewayAddress;
	private String bleName;
	private byte[] bleData;

	@SuppressWarnings("unused")
	private Packet(){}

	public Packet(byte[] packet){
		setPacketNumber(Arrays.copyOfRange(packet, 0, 2));//first 2 bytes include packet number
		setBleAddress(Arrays.copyOfRange(packet, 2, 8));//bytes 2 to 7 are BLE MAC address
		setSignalStrength(packet[8]);// byte 8 is radio signal strength
		setGatewayAddress(Arrays.copyOfRange(packet, 78, 84));//bytes 78 to 83 are gateway MAC address

		this.broadcast = getPacket(packet, 10);//bytes 10 to 41 are broadcast data. Include manufacturer data. Not needed now. Stripped from unneeded zeros.

		byte[] response = getPacket(packet, 44);//bytes 44 to 77 include response data. Stripping down from unneeded zeros.
		setBleName(response);
		setBleData(response);

		//there's also checksum at the end of packet, but it's not that important to check if it's correct.
	}

	public int getPacketNumber() {
		return packetNumber;
	}

	public void setPacketNumber(byte[] packetNumber) {
		this.packetNumber = (int)(((packetNumber[1]&0xff)<<8)|(packetNumber[0]&0xff));
	}

	public String getBleAddress() {
		return bleAddress;
	}

	public void setBleAddress(byte[] bleAddress) {
		String address = "";
		for(int i=bleAddress.length-1 ; i>=0 ; i--){
			address += Utils.byteToString(bleAddress[i]);
		}
		this.bleAddress = address;
	}

	public byte getSignalStrength() {
		return signalStrength;
	}

	public void setSignalStrength(byte signalStrength) {
		this.signalStrength = signalStrength;
	}

	public byte[] getBroadcast() {
		return broadcast;
	}

	public void setBroadcast(byte[] broadcast) {
		this.broadcast = broadcast;
	}

	public String getGatewayAddress() {
		return gatewayAddress;
	}

	public void setGatewayAddress(byte[] gatewayAddress) {
		String address="";
		for(int i=0; i<6; i++){
			address += Utils.byteToString(gatewayAddress[i]);
		}

		this.gatewayAddress = address;
	}

	private byte[] getPacket(byte[] packet, int offset){
		int len = (int)(((packet[offset+1]&0xff)<<8)|(packet[offset]&0xff));

		if(len>0){
			return Arrays.copyOfRange(packet, offset+2, offset+2+len);
		}return null;
	}

	public String getBleName() {
		return bleName;
	}

	public void setBleName(byte[] response) {
		if(response!=null){
			int len = (response[0]&0xff);
			if(len>0){
				this.bleName = new String(response, 2, len-1);//reading from 2nd byte, because byte 1 includes tab sign.
				return;
			}
		}
		this.bleName = "";
	}

	public byte[] getBleData() {
		return bleData;
	}

	public void setBleData(byte[] response) {
		if(response!=null){
			int offset = 1+(response[0]&0xff);//1 byte after name field
			int len = (response[offset]&0xff);

			if(len>0){
				this.bleData = Arrays.copyOfRange(response, offset+1, offset+1+len);//copy bytes from offset+1 (offset byte is data length)
				return;
			}
		}
		this.bleData = null;
	}
}
