package ztbd.project.bean;

public class Config {
	private String device_id; 
	private double sensor_low;
	private double sensor_high;
	private int schedule;
	
	public Config(String device_id, double sensor_low, double sensor_high, int schedule){
		this.device_id = device_id;
		this.sensor_high = sensor_high;
		this.sensor_low = sensor_low;
		this.schedule = schedule;
	}
	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public double getSensor_low() {
		return sensor_low;
	}
	public void setSensor_low(double sensor_low) {
		this.sensor_low = sensor_low;
	}
	public double getSensor_high() {
		return sensor_high;
	}
	public void setSensor_high(double sensor_high) {
		this.sensor_high = sensor_high;
	}
	public int getSchedule() {
		return schedule;
	}
	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}
	
	
	
}
