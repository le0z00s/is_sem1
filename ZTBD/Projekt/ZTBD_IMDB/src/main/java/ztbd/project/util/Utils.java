package ztbd.project.util;

import ztbd.project.packet.Packet;

public class Utils {
	public static String byteToString(byte b){
		StringBuilder sb = new StringBuilder();
		sb.append(Integer.toHexString(b&0xff));
		if (sb.length() < 2) {
			sb.insert(0, '0'); // pad with leading zero if needed
		}
		String hex = sb.toString();
		return hex;
	}

	public static byte[] hexStringToByteArray(String s){
		int len = s.length();
		byte[] data = new byte[len/2];
		for(int i=0; i<len; i+=2){
			data[i/2] = (byte)((Character.digit(s.charAt(i), 16)<<4)+Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

	public static boolean isEfento(Packet packet){
		if(packet.getBleData()!=null && packet.getBleData().length==13){
			return (int)(((packet.getBleData()[1]&0xff))|(packet.getBleData()[2]&0xff)<<8) == 0x026c ? true : false;//check if BLE data starts with 026c
		}return false;
	}
}
