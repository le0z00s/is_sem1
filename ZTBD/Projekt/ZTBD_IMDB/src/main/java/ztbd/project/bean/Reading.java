package ztbd.project.bean;

import java.util.Date;

public class Reading {
	private String serial;
	private byte signalStrength;
	private Date date;
	private double temperature;
	private int batLevel;
	
	public Reading(String serial, byte signalStrength, Date date, double temperature, int batLevel){
		this.batLevel = batLevel;
		this.date = date;
		this.serial = serial;
		this.signalStrength = signalStrength;
		this.temperature = temperature;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public byte getSignalStrength() {
		return signalStrength;
	}

	public void setSignalStrength(byte signalStrength) {
		this.signalStrength = signalStrength;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public int getBatLevel() {
		return batLevel;
	}

	public void setBatLevel(int batLevel) {
		this.batLevel = batLevel;
	}
	
	
}
