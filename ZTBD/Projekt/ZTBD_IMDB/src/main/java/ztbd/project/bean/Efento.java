package ztbd.project.bean;

import java.util.Arrays;

public class Efento {
	private String serial;
	private byte signalStrength;
	private String name;
	private int counter;
	private double humidity;
	private double pressure;
	private double temperature;
	private int period;
	private byte version;
	private int batLevel;
	
	@SuppressWarnings("unused")
	private Efento(){}
	
	public Efento(String serial, String name, byte signalStrength, byte[] data){
		this.serial = serial;
		this.signalStrength = signalStrength;
		this.name = name;
		this.version = data[11];
		
		setCounter(data[4]);
		setHumidity(data[5]);
		setPressure(data[6]);
		setTemperature(Arrays.copyOfRange(data, 7, 9));
		setPeriod(Arrays.copyOfRange(data, 9, 11));
		setBatLevel(data[12]);
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public byte getSignalStrength() {
		return signalStrength;
	}

	public void setSignalStrength(byte signalStrength) {
		this.signalStrength = signalStrength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(byte counter) {
		this.counter = (int)(counter&0xff);
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(byte humidity) {
		//TODO add setHumidity method
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(byte pressure) {
		//TODO add setPressure method
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(byte[] temperature) {
		int temp = (int)(((temperature[1]&0xff)<<8)|(temperature[0]&0xff));
		this.temperature = (temp - 32767)/100.0;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(byte[] period) {
		this.period = (int)(((period[1]&0xff)<<8)|(period[0]&0xff));
	}

	public byte getVersion() {
		return version;
	}

	public void setVersion(byte version) {
		this.version = version;
	}

	public int getBatLevel() {
		return batLevel;
	}

	public void setBatLevel(byte batLevel) {
		this.batLevel = (int)(batLevel&0xff) * 4 +2800;
	}	
}
