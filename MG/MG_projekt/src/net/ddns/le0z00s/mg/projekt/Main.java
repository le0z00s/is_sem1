package net.ddns.le0z00s.mg.projekt;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main implements IMain{

	private static String propPath = "settings.ini";
	
	private static List<Point2D.Double> vertices;
	private static List<List<Integer>> triangles;
	
	public static void main(String[] args) {
		Main main = new Main();
		if(main.init(args)){
			String input = main.getProperty("input", "res/data.txt");
			String output = main.getProperty("output", "res/chart.ps");
			String program = main.getProperty("execute", "xdg-open "+output);
			boolean wholeGrid = Boolean.getBoolean(
					main.getProperty("grid", "false"));
			boolean printVertices = Boolean.getBoolean(
					main.getProperty("vertices", "true"));
			boolean legend = Boolean.getBoolean(
					main.getProperty("legend", "true"));
			int vertex = Integer.parseInt(args[0]);
			
			try {
				main.getData(input);
				
				Triangulation triangulation = new Triangulation(
						vertices, triangles, vertex);
				triangulation.generate(output, wholeGrid, printVertices, legend);
				main.execute(program);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public String getProperty(String key, String defaultValue) {
		Properties prop = new Properties();
		Path path = Paths.get(propPath);
		InputStream input = null;
		try{
			input = Files.newInputStream(path, StandardOpenOption.CREATE);
			prop.load(input);
			input.close();
		} catch (IOException e) {
			//store value - mostly redundant, but I'm lazy
			OutputStream out;
			try {
				out = Files.newOutputStream(path);
				prop.setProperty(key, defaultValue);
				prop.store(out, null);
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return prop.getProperty(key, defaultValue);
	}

	@Override
	public void getData(String file) throws FileNotFoundException {
		Scanner in = new Scanner(new File(file));
        List<String> data = new ArrayList<>(); 

        while(in.hasNextLine()) {
            String linia = in.nextLine();
            if(!linia.matches("\\s+") && !linia.isEmpty()) {
                data.add(linia);
            }
        }
        
        int verticesAmount = Integer.valueOf(data.get(0)) + 1;
        vertices = new ArrayList<Point2D.Double>(verticesAmount);
        int index = verticesAmount + 1;
        int j;
        String regex = "\\s+";
        for(j = 1 ; j < index ; j++){
        	String coordinates[] = data.get(j).split(regex);
        	double x = Double.valueOf(coordinates[0]);
        	double y = Double.valueOf(coordinates[1]);
        	vertices.add(new Point2D.Double(x, y));
        }
        
        int triangleAmount = Integer.valueOf(data.get(j)) + 1;
        triangles = new ArrayList<List<Integer>>(triangleAmount);
        index += triangleAmount + 1;
        
        for(++j; j < index ; j++){
        	String[] vert = data.get(j).split(regex);
        	List<Integer> verticesList = new ArrayList<Integer>(3);
            for (String vertexNumber : vert) {
                int n = Integer.valueOf(vertexNumber);
                verticesList.add(n);
            }

            triangles.add(verticesList);
        }
        
        in.close();
	}

	@Override
	public void execute(String command) throws IOException {
		Runtime.getRuntime().exec(command);
	}

	@Override
	public boolean init(String[] args) {
        if (args.length != 1) 
        {
            System.out.println("Nie wlasciwa liczba argumentow. "
                    + "Prawidlowe wywolanie: java Main <numer_punktu>");
            return false;
        }
        return true;
	}

}
