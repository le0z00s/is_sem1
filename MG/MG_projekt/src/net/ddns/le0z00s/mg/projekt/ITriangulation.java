package net.ddns.le0z00s.mg.projekt;

import java.io.FileNotFoundException;

public interface ITriangulation {

	void generate(String file, boolean wholeGrid, boolean vertices,
			boolean legend) throws FileNotFoundException;
		
}
