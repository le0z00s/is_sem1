package net.ddns.le0z00s.mg.projekt;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IMain {
	/**
	 * Reads property from file.
	 * Creates file if not exists and stores values.
	 * @param key Property name
	 * @param defaultValue Default value
	 * @return Value or default value
	 */
	String getProperty(String key, String defaultValue);
	
	/**
	 * Reads data from input file.
	 * Stores data in two lists representing
	 * vertices (with their coordinates) and 
	 * triangles (with numbers of vertices)
	*/
	void getData(String file) throws FileNotFoundException;
	
	void execute(String command) throws IOException;
	
	boolean init(String[] args);
	
}
