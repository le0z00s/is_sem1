package net.ddns.le0z00s.mg.lab6;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Class generating pie chart based on input data.
 * @author Rafał Zbojak
 *
 */
public class Chart {

	private static Map<String, Series> chartData = new HashMap<String, Series>();
	private static int[] chartCenter = {250, 250};
	private static int[] fontSize = {28, 16};
	private static int radius = 150;
	private static int margin = 50;
	
	/**
	 * Main constructor.
	 * Handles creating of a chart.
	 * @param readFile Raw input data
	 */
	public Chart(Map<String, Double> readFile) {
		initData(readFile);
	}

	/**
	 * Returns sum of all values in the input map
	 * @param input Pre-processed input data
	 * @return Sum of all values
	 */
	private static double getOveralValue(Map<String, Double> input){
		double overalValue = 0.0;

		for(Map.Entry<String, Double> row : input.entrySet()){
			overalValue += row.getValue();
		}

		return	overalValue;
	}

	/**
	 * Prepares data for chart
	 * @param rawData Raw data for chart
	 */
	private static void initData(Map<String, Double> rawData){
		List<Color> colors = generateColors(rawData.size());
		double overalValue = getOveralValue(rawData);
		int colorIndex = 0;
		
		for(Map.Entry<String, Double> entry : rawData.entrySet()){
			double value = entry.getValue()/overalValue;
			Color color = colors.get(colorIndex);
			chartData.put(entry.getKey(), new Series(value, color));
			colorIndex++;
		}
	}
	
	/**
	 * Generate list of colors
	 * @param amount Number of colors to generate
	 * @return List of colors
	 */
	private static List<Color> generateColors(int amount){
		List<Color> colors = new ArrayList<Color>();

		Random random = new Random();
		Color color;
		int r, g, b;
		for(int i=0 ; i<amount ; i++){
			do{
				r = random.nextInt(255);
				g = random.nextInt(255);
				b = random.nextInt(255);
				color = new Color(r, g, b);
			}while (colorExists(color, colors));
			
			colors.add(color);
			
		}

		return colors;
	}

	/**
	 * Checks if given color already exists
	 * @param color Color to check
	 * @param colors List of available colors
	 * @return true/false
	 */
	private static boolean colorExists(Color color, List<Color> colors){
		for(Color tmp : colors){
			if(tmp.equals(color)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Generates output file.
	 * @param path Output file path
	 * @throws FileNotFoundException 
	 */
	public void generateFile(String path) throws FileNotFoundException{
		PrintWriter out = new PrintWriter(path);
		out.println("%! Adobe – PS");
		out.println("%%BoundingBox: 0 0 750 500");
		
		generateChart(out);
		generateLegend(out);
		
		out.println("showpage");
		out.close();
	}
	
	/**
	 * Generates chart
	 * @param out Output file
	 */
	private void generateChart(PrintWriter out){
		double startingAngle = 0, endAngle;
		
		for(Map.Entry<String, Series> entry : chartData.entrySet()){
			Color color = entry.getValue().getColor();
			Double angle = entry.getValue().getValue() * 360;
			endAngle = startingAngle + angle;
			double r = color.getRed() / 255.0;
			double g = color.getGreen() / 255.0;
			double b = color.getBlue() / 255.0;
			
			
			out.println("newpath");
			out.format("%d %d moveto\n", chartCenter[0], chartCenter[1]);
			out.format(Locale.US, "%.2f %.2f %.2f setrgbcolor\n", 
					r, g, b);
			out.format(Locale.US, "%d %d %d %.2f %.2f arc\n", 
					chartCenter[0], chartCenter[1],	
					radius, startingAngle, endAngle);
			out.println("closepath");
			out.println("fill");
			out.println("gsave");
			out.println("grestore");
			out.println("stroke");
			
			startingAngle = endAngle;
		}
	}

	/**
	 * Generates legend for pie chart
	 * @param out Output file
	 */
	private void generateLegend(PrintWriter out){
		out.println("/Times-Bold findfont");
		out.format("%d scalefont\n", fontSize[0]);
		out.println("setfont");
		out.println("newpath");
		
		Integer x = chartCenter[0] + radius + margin;
		Integer y = chartCenter[1] + radius;
		
		out.format("%d %d moveto\n", x, y);
		
		out.format(Locale.US, "%.2f %.2f %.2f setrgbcolor\n", 
				0.0, 0.0, 0.0);
		out.println("(Legenda:) show");
		out.println("/Verdana findfont");
		out.format("%d scalefont\n", fontSize[1]);
		out.println("setfont");
		
		y -= fontSize[0];
		for(Map.Entry<String, Series> entry : chartData.entrySet()){
			out.format("%d %d moveto\n", x, y);
			
			Color color = entry.getValue().getColor();
			double r = color.getRed() / 255.0;
			double g = color.getGreen() / 255.0;
			double b = color.getBlue() / 255.0;
			
			out.format(Locale.US, "%.2f %.2f %.2f setrgbcolor\n", 
					r, g, b);
			out.format("(%s) show\n", entry.getKey());
			out.format(Locale.US, "%.2f %.2f %.2f setrgbcolor\n", 
					0.0, 0.0, 0.0);
			
			double percentage = entry.getValue().getValue() * 100;
			
			out.format("( - %.2f%%) show\n", percentage);			
			
			y -= fontSize[1];
		}
	}
	
	/**
	 * Executes shell command.
	 * Opens output file in given editor.
	 * @param command Command to execute
	 * @throws IOException 
	 */
	public void execute(String command) throws IOException{
		Runtime.getRuntime().exec(command);
	}
}
