package net.ddns.le0z00s.mg.lab6;

import java.awt.Color;

/**
 * Class representing single series data
 * @author Rafał Zbojak
 */
public class Series {
	private double value;
	private Color color;
	
	public Series(double value, Color color){
		this.setValue(value);
		this.setColor(color);
	}

	public Double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}
