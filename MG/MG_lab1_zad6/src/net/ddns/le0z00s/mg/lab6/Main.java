package net.ddns.le0z00s.mg.lab6;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Main class of the program.
 * Loads properties file where filenames for input and output 
 * data are stored. Properties file also contains path to
 * program which will be executed on this program termination.
 * 
 * After loading data, it makes pie chart and executes preview of the chart.
 * @author Rafał Zbojak
 */
public class Main {
	
	private static String propPath = "settings.ini";
	
	
	public static void main(String[] args){
		Main main = new Main();
		String input = main.getProperty("input", "res/data.txt");
		String output = main.getProperty("output", "res/chart.ps");
		String program = main.getProperty("execute", "xdg-open "+output);
		try {
			Chart chart = new Chart(main.readFile(input));
			chart.generateFile(output);
			chart.execute(program);
		} catch (IOException e) {
			e.printStackTrace();
		}
				
	}
	
	/**
	 * Reads data from input file.
	 * Input file must contain series of data in separate lines.
	 * Data for a series should be separates by white space.
	 * First element in the line is a name of the series
	 * followed by its value.
	 * @param file Path to file
	 * @return Map containing series data.
	 * @throws IOException 
	 */
	private Map<String, Double> readFile(String file) throws IOException{
		Map<String, Double> map = new HashMap<String, Double>();
		Path path = Paths.get(file);
		Charset cs = StandardCharsets.UTF_8;	
		String regex = "\\s+";	
		
		List<String> tmp = Files.readAllLines(path, cs);
		
		for(String line : tmp){
			String[] data = line.split(regex);
			int last = data.length - 1;
			String key = data[0];
			//Build label for hashmap
			for(int i=1 ; i<last ; i++){
				key += " " + data[i];
			}
			data[last] = data[last].replace(',', '.');
			map.put(key.toUpperCase(), Double.parseDouble(data[last]));
		}
		
		return map;
	}
	
	/**
	 * Reads property from file.
	 * Creates file if not exists and stores values.
	 * @param key Property name
	 * @param defaultValue Default value
	 * @return Value or default value
	 */
	private String getProperty(String key, String defaultValue){
		Properties prop = new Properties();
		Path path = Paths.get(propPath);
		InputStream input = null;
		try{
			input = Files.newInputStream(path, StandardOpenOption.CREATE);
			prop.load(input);
			input.close();
		} catch (IOException e) {
			//store value - mostly redundant, but I'm lazy
			OutputStream out;
			try {
				out = Files.newOutputStream(path);
				prop.setProperty(key, defaultValue);
				prop.store(out, null);
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return prop.getProperty(key, defaultValue);
	}
	
	
}
