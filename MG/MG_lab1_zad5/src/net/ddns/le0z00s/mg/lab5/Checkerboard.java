package net.ddns.le0z00s.mg.lab5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Generates PS file containinx XxY checherboard
 * @author Rafał Zbojak
 */
public class Checkerboard {
	private final int rows;
	private final int columns;
	private final String fileName;
	
	public Checkerboard(int rows, int columns, String fileName){
		this.rows = rows;
		this.columns = columns;
		this.fileName = fileName;
	}
	
	public void generate() throws FileNotFoundException{
		PrintWriter out = new PrintWriter(fileName);
		out.println("%! Adobe – PS");
		out.println("%%BoundingBox: 0 0 500 500");
		out.format("/edge1 {500 %d div} def\n", rows);
		out.format("/edge2 {-500 %d div} def\n", rows);
		out.println("/color1{0.5 0.5 setgray} def");
		out.println("/color2{0.75 0.75 setgray} def");
		out.println("/square{0 0 moveto edge1 0 rlineto 0 edge1 rlineto edge2 0 rlineto closepath} def");
		
		boolean oddColumn = false;
		boolean oddRow;
		for(int i=0; i<rows; i++){
			oddRow = !oddColumn;
			out.println("gsave");
			for (int j = 0; j<columns; j++) {
				if(oddColumn){
					out.println("color1 square fill edge1 0 translate");
				}else {
					out.println("color2 square fill edge1 0 translate");
				}
				oddColumn = !oddColumn;
			}
			out.println("grestore 0 edge1 translate");
			oddColumn = oddRow;
		}
		out.println("showpage");
		out.close();
	}
}
