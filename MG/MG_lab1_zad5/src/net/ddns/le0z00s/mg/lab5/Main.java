package net.ddns.le0z00s.mg.lab5;

import java.io.FileNotFoundException;

/**
 * Generates PS file containinx XxY checherboard
 * @author Rafał Zbojak
 */
public class Main {

	public static void main(String[] args) {
		int rows = Integer.parseInt(args[0]);
		int columns = Integer.parseInt(args[1]);
		Checkerboard checkerboard = new Checkerboard(rows, columns, "lab5.ps");
		try {
			checkerboard.generate();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
