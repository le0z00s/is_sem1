/*
 * Copyright (C) 2016 Rafał Zbojak
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.zbojak.mpd.postscript;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import pl.zbojak.mpd.scheduling.DMS;
import pl.zbojak.mpd.task.Task;

/**
 *
 * @author Rafał Zbojak
 */
public class PostScript {
    private DMS dms;
    private final int[] PAGE_SIZE = {800, 600};
    private final int FONT_SIZE = 10;
    private final int MARGIN = 20;
    
    private int[] TASK_SIZE = new int[2];
    private int[] POSITION = new int[2];
    
    //Colors for the plot
    private final String RED = "0.5 0 0";
    private final String GREEN = "0 0.5 0";
    private final String BLUE = "0 0.69 1";
    
    
    /**
     * Ensure that default constructor is unavailable
     */
    private PostScript(){}
    
    public PostScript(DMS dms){
        this.dms = dms;
    }
    
    public void generate(String filename) throws FileNotFoundException{
        PrintWriter writer = new PrintWriter(filename);
        header(writer);
        initCoordinates();
        legend(writer);
        
        int size = dms.getTasks().size();
        for(int i=0 ; i < size ; i++){
            
            taskChart(writer, i);
            task(writer, i);
            
        }
                
        
        writer.println("showpage");
        writer.close();
    }
    
    private void header(PrintWriter writer){
        writer.println("%! Adobe PS");
        writer.format("%%%%BoundingBox: 0 0 %d %d\n", PAGE_SIZE[0], PAGE_SIZE[1]);
        writer.println("/Times-Roman findfont");
        writer.format("%d scalefont\n", FONT_SIZE);
        writer.println("setfont");
    }
    
    private void initCoordinates(){
        this.POSITION[0] = this.MARGIN;
        this.POSITION[1] = this.PAGE_SIZE[1] - 2*MARGIN;
        this.TASK_SIZE[0] = (this.PAGE_SIZE[0] - 2*MARGIN) / dms.getSIMULATION_TIME();
        this.TASK_SIZE[1] = (int) (this.PAGE_SIZE[1] - 100) / ((this.FONT_SIZE / 2) * dms.getTasks().size());
    }
    
    private void legend(PrintWriter writer){
        int[] textPosition = new int[2];
        textPosition[0] = this.POSITION[0] + MARGIN/2;
        textPosition[1] = this.POSITION[1] + this.FONT_SIZE/2;
        
        writer.format("%s setrgbcolor\n", this.RED);
        writer.format("%d %d moveto\n", this.POSITION[0], this.POSITION[1]);
        writer.format("0 %d rlineto\n", this.FONT_SIZE*2);
        writer.println("stroke");
        writer.format("%d %d moveto\n", textPosition[0], textPosition[1]);
        writer.println("(DEADLINE) show");
        
        this.POSITION[1] -= this.FONT_SIZE * 2.5;
        
        textPosition[1] = this.POSITION[1] + this.FONT_SIZE/2;
        
        writer.format("%s setrgbcolor\n", this.GREEN);
        writer.format("%d %d moveto\n", this.POSITION[0], this.POSITION[1]);
        writer.format("0 %d rlineto\n", this.FONT_SIZE*2);
        writer.println("stroke");
        writer.format("%d %d moveto\n", textPosition[0], textPosition[1]);
        writer.println("(PERIOD) show");
        
        this.POSITION[1] -= this.FONT_SIZE + this.TASK_SIZE[1];
        
        textPosition[0] = this.POSITION[0] + this.TASK_SIZE[0] + this.MARGIN / 2;
        textPosition[1] = this.POSITION[1];
        
        writer.format("%s setrgbcolor\n", this.BLUE);
        writer.println("newpath");
        writer.format("%d %d moveto\n", this.POSITION[0], this.POSITION[1]);
        writer.format("%d 0 rlineto\n", this.TASK_SIZE[0]);
        writer.format("0 %d rlineto\n", this.TASK_SIZE[1]);
        writer.format("-%d 0 rlineto\n", this.TASK_SIZE[0]);
        writer.println("closepath");
        writer.println("fill");
        writer.format("%d %d moveto\n", textPosition[0], textPosition[1]);
        writer.println("(TASK) show");
        
        this.POSITION[1] -= this.FONT_SIZE * 2.5;
    }
    
    private void taskChart(PrintWriter writer, int index){
        
        int xAxisLenght = dms.getSIMULATION_TIME() * this.TASK_SIZE[0];
        int yAxisHeight = this.TASK_SIZE[1] * 2;
        
        this.POSITION[1] -= (this.MARGIN + yAxisHeight);
        
        writer.println("0 setgray");
        writer.format("%d %d moveto\n", this.POSITION[0], this.POSITION[1]);
        writer.format("%d 0 rlineto\n", xAxisLenght);
        writer.println("stroke");
        
        int xAxisEnd = this.PAGE_SIZE[0] - this.MARGIN;
        int yAxisEnd = this.POSITION[1] + yAxisHeight;
        int[] arrow = new int[2];
        arrow[0] = (int) (0.01 * xAxisLenght);
        arrow[1] = (int) (0.05 * yAxisHeight);
        
        writer.format("%d %d moveto\n", xAxisEnd, this.POSITION[1]);
        writer.format("-%d -%d rlineto\n", arrow[0], arrow[1]);
        writer.println("stroke");
        writer.format("%d %d moveto\n", xAxisEnd, this.POSITION[1]);
        writer.format("-%d %d rlineto\n", arrow[0], arrow[1]);
        writer.println("stroke");
        
        writer.format("%d %d moveto\n", this.POSITION[0], this.POSITION[1]);
        writer.format("0 %d rlineto\n", yAxisHeight);
        writer.println("stroke");
        
        writer.format("%d %d moveto\n", this.POSITION[0], yAxisEnd);
        writer.format("-%d -%d rlineto\n", arrow[1], arrow[0]);
        writer.println("stroke");
        writer.format("%d %d moveto\n", this.POSITION[0], yAxisEnd);
        writer.format("%d -%d rlineto\n", arrow[1], arrow[0]);
        writer.println("stroke");
        
        int[] textPos = new int[2];
        textPos[0] = this.POSITION[0] - this.FONT_SIZE;
        textPos[1] = this.POSITION[1] + (yAxisHeight / 2) - (this.FONT_SIZE / 2);
        writer.format("%d %d moveto\n", textPos[0], textPos[1]);
        writer.format("(%d) show\n", index + 1);
        
        Task task = dms.getTasks().get(index);
        int[] labelPos = new int[2];
        
        int deadline;
        int periodCount = 0;
        for(int t = 1 ; t <= dms.getSIMULATION_TIME() ; t++){
            labelPos[0] = this.POSITION[0] + t * this.TASK_SIZE[0];
            labelPos[1] = this.POSITION[1] + this.TASK_SIZE[1] * 2;
            deadline = periodCount*task.getPeriod() + task.getDeadline();
            if(t % task.getPeriod() == 0 && task.getPeriod() != task.getDeadline()){
                writer.format("%s setrgbcolor\n", this.GREEN);
                writer.format("%d %d moveto\n", labelPos[0], labelPos[1]);
                writer.format("0 -%d rlineto\n", yAxisHeight + this.FONT_SIZE / 2);
                writer.println("stroke");
                periodCount++;
            }
            else if(t == deadline){
                writer.format("%s setrgbcolor\n", this.RED);
                writer.format("%d %d moveto\n", labelPos[0], labelPos[1]);
                writer.format("0 -%d rlineto\n", yAxisHeight + this.FONT_SIZE / 2);
                writer.println("stroke");
            }
            else{
                writer.println("0 setgray");
            }
            
            labelPos[1] = this.POSITION[1];
            textPos[0] = labelPos[0] - (this.TASK_SIZE[0] / 2) - (this.FONT_SIZE / 2);
            textPos[1] = labelPos[1] - this.FONT_SIZE;
            
            writer.format("%d %d moveto\n", labelPos[0], labelPos[1]);
            writer.format("0 -%d rlineto\n", this.FONT_SIZE / 2);
            writer.println("stroke");
            writer.format("%d %d moveto\n", textPos[0], textPos[1]);
            writer.format("(%d) show\n", t);
        }
    }
    
    private void task(PrintWriter writer, int index){
        int orderIndex;
        int[] taskPos = new int[2];
        taskPos[1] = this.POSITION[1] + 1;
        
        writer.format("%s setrgbcolor\n", this.BLUE);
        
        for(int t = 0 ; t < dms.getSIMULATION_TIME() ; t++){
            orderIndex = dms.getSimulationOrder().get(t);
            
            if(orderIndex == (index + 1)){
                taskPos[0] = this.POSITION[0] + t * this.TASK_SIZE[0];        
                writer.println("newpath");
                writer.format("%d %d moveto\n", taskPos[0], taskPos[1]);
                writer.format("%d 0 rlineto\n", this.TASK_SIZE[0]);
                writer.format("0 %d rlineto\n", this.TASK_SIZE[1]);
                writer.format("-%d 0 rlineto\n", this.TASK_SIZE[0]);
                writer.println("fill");
            }
        }
    }
}
