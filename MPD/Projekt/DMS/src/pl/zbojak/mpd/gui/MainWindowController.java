/*
 * Copyright (C) 2016 rav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.zbojak.mpd.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.zbojak.mpd.postscript.PostScript;
import pl.zbojak.mpd.scheduling.DMS;
import pl.zbojak.mpd.task.Task;
import pl.zbojak.mpd.task.TaskFactory;

/**
 * FXML Controller class
 *
 * @author rav
 */
public class MainWindowController implements Initializable {

    private DMS dms;
    private static String propPath = "settings.ini";

    @FXML
    private TableView<Task> taskTable;
    @FXML
    private TableColumn<Task, String> idColumn;
    @FXML
    private TableColumn<Task, String> durationColumn;
    @FXML
    private TableColumn<Task, String> periodColumn;
    @FXML
    private TableColumn<Task, String> deadlineColumn;

    private ObservableList<Task> tasks = FXCollections.observableArrayList();

    @FXML
    public void reset() {
        this.dms = new DMS();
        tasks.clear();
    }

    @FXML
    public void exit() {
        System.exit(-1);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reset();
        taskTable.setItems(tasks);

        idColumn.setCellValueFactory(
                new PropertyValueFactory<Task, String>("taskNumber")
        );

        durationColumn.setCellValueFactory(
                new PropertyValueFactory<Task, String>("duration")
        );

        deadlineColumn.setCellValueFactory(
                new PropertyValueFactory<Task, String>("deadline")
        );

        periodColumn.setCellValueFactory(
                new PropertyValueFactory<Task, String>("period")
        );
    }

    @FXML
    public void load() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load dataset");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Datasets", "*.set"));
        File file = fileChooser.showOpenDialog(null);
        if (!file.equals(null)) {
            getDataFromFile(file.getAbsolutePath());
        }
    }

    @FXML
    public void save() throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save dataset");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Datasets", "*.set"));
        File file = fileChooser.showSaveDialog(null);
        if (!file.equals(null)) {
            saveDataToFile(file.getAbsolutePath());
        }
    }

    @FXML
    public void delete() {
        int rows = dms.getTasks().size();

        if (rows > 0) {
            dms.getTasks().remove(rows - 1);
            tasks.remove(rows - 1);
        }
    }

    @FXML
    public void add() {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add Task");
        window.setWidth(600.0);
        window.setHeight(150.0);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        Label durationLabel = new Label("Task Duration");
        Label periodLabel = new Label("Period");
        Label deadlineLabel = new Label("Deadline");
        TextField duration = new TextField();
        TextField period = new TextField();
        TextField deadline = new TextField();
        duration.setPadding(new Insets(0, 10, 0, 10));
        period.setPadding(new Insets(0, 10, 0, 10));
        deadline.setPadding(new Insets(0, 10, 0, 10));

        HBox buttonBox = new HBox(10);
        Button okButton = new Button("OK");
        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> window.close());

        okButton.setOnAction(e -> {
            String durationText = duration.getText();
            String periodText = period.getText();
            String deadlineText = deadline.getText();
            boolean durationEmpty = durationText.isEmpty();
            boolean periodEmpty = periodText.isEmpty();
            boolean deadlineEmpty = deadlineText.isEmpty();

            if (durationEmpty || periodEmpty || deadlineEmpty) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "One of the fields is empty!", ButtonType.OK);
                alert.showAndWait();
            } else if (validate(durationText)
                    && validate(periodText) && validate(deadlineText)) {
                int taskNumber = dms.getTasks().size();
                TaskFactory task = new TaskFactory();
                task.setTaskNumber(taskNumber + 1);
                task.setDeadline(Integer.parseInt(deadlineText));
                task.setDuration(Integer.parseInt(durationText));
                task.setPeriod(Integer.parseInt(periodText));

                dms.getTasks().add(task.create());
                task.setDuration(task.getDuration() + 2);
                tasks.add(task.create());

                window.close();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "All fields must contain numbers!", ButtonType.OK);
                alert.showAndWait();
            }
        });

        buttonBox.getChildren().addAll(okButton, cancelButton);

        grid.add(durationLabel, 0, 0);
        grid.add(deadlineLabel, 1, 0);
        grid.add(periodLabel, 2, 0);
        grid.add(duration, 0, 1);
        grid.add(deadline, 1, 1);
        grid.add(period, 2, 1);
        grid.add(buttonBox, 2, 2);

        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.showAndWait();

    }

    @FXML
    public void simulate() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Set Simulation Time");
        window.setWidth(600.0);
        window.setHeight(150.0);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        Label timeLabel = new Label("Task Duration");
        TextField time = new TextField();
        time.setPadding(new Insets(0, 10, 0, 10));

        HBox buttonBox = new HBox(10);
        Button okButton = new Button("OK");
        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> window.close());

        okButton.setOnAction(e -> {
            String timeText = time.getText();
            boolean timeEmpty = timeText.isEmpty();

            if (timeEmpty) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "One of the fields is empty!", ButtonType.OK);
                alert.showAndWait();
            } else if (validate(timeText)) {
                int simulationTime = Integer.parseInt(timeText);

                dms.setSIMULATION_TIME(simulationTime);
                dms.simulate();

                window.close();
                try {
                    saveChart();
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "All fields must contain numbers!", ButtonType.OK);
                alert.showAndWait();
            }
        });

        buttonBox.getChildren().addAll(okButton, cancelButton);

        grid.add(timeLabel, 1, 0);
        grid.add(time, 1, 1);
        grid.add(buttonBox, 2, 2);

        Scene scene = new Scene(grid);
        window.setScene(scene);
        window.showAndWait();
    }

    private void getDataFromFile(String path) throws FileNotFoundException, IOException {
        reset();
        List<String> list = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String line;
        String regex = "\\s+";

        tasks.clear();

        while ((line = reader.readLine()) != null) {
            list.add(line);
        }
        reader.close();

        String[] parts;
        TaskFactory task;
        int taskNumber = 1;
        for (String row : list) {
            parts = row.split(regex);
            if (parts.length != 3) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Encountered error while loading input file", ButtonType.OK);
                alert.showAndWait();
                if (alert.getResult() == ButtonType.OK) {
                    break;
                }
            } else {
                task = new TaskFactory();
                task.setTaskNumber(taskNumber);
                task.setDuration(Integer.parseInt(parts[0]));
                task.setPeriod(Integer.parseInt(parts[1]));
                task.setDeadline(Integer.parseInt(parts[2]));
                dms.addTask(task.create());
                task.setDuration(Integer.parseInt(parts[0]) + 2);
                tasks.add(task.create());
                taskNumber++;
            }
        }
    }

    private void saveDataToFile(String path) throws FileNotFoundException {
        PrintWriter out = new PrintWriter(path);
        for (Task task : dms.getTasks()) {
            out.format("%d %d %d\n", task.getDuration(), task.getPeriod(), task.getDeadline());
        }
        out.close();
    }

    private boolean validate(String value) {
        return value.matches("[0-9]*");
    }

    private void saveChart() throws FileNotFoundException, IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Gant Chart");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PostScript", "*.ps"));
        File file = fileChooser.showSaveDialog(null);
        String path = null;
        if (!file.equals(null)) {
            PostScript ps = new PostScript(dms);
            ps.generate(file.getAbsolutePath());
            path = file.getAbsolutePath();
            
            String program = this.getProperty("postscript_reader", "xdg-open");
            String command = String.format("%s %s", program, path);
            execute(command);
        }
    }

    public void execute(String command) throws IOException {
        Runtime.getRuntime().exec(command);
    }

    private String getProperty(String key, String defaultValue){
		Properties prop = new Properties();
		Path path = Paths.get(propPath);
		InputStream input = null;
		try{
			input = Files.newInputStream(path, StandardOpenOption.CREATE);
			prop.load(input);
			input.close();
		} catch (IOException e) {
			//store value - mostly redundant, but I'm lazy
			OutputStream out;
			try {
				out = Files.newOutputStream(path);
				prop.setProperty(key, defaultValue);
				prop.store(out, null);
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return prop.getProperty(key, defaultValue);
	}
}
