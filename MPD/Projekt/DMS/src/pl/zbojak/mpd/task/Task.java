/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zbojak.mpd.task;

/**
 *
 * @author Rafał Zbojak
 */
public class Task implements Comparable<Task>{
    private final int deadline;
    private final int period;
    private final int duration;
    private final int taskNumber;
    
    private int occurrences;
    private Integer executionTime;
    private int priority;
    
    Task(TaskFactory task){
        this.deadline = task.getDeadline();
        this.duration = task.getDuration();
        this.period = task.getPeriod();
        this.taskNumber = task.getTaskNumber();
        
        this.occurrences = 0;
    }

    public int getDeadline() {
        return deadline;
    }

    public int getPeriod() {
        return period;
    }

    public int getDuration() {
        return duration;
    }

    public int getTaskNumber() {
        return taskNumber;
    }

    public int getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(int occurrences) {
        this.occurrences = occurrences;
    }
    
    public void addOccurancy(){
        this.occurrences++;
    }

    public Integer getExecutionTime() {
        if(executionTime == null){
            return Integer.MAX_VALUE;
        }
        
        return executionTime;
    }

    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(Task o) {
        if(this.getPriority() < o.getPriority()){
            return 1;
        }else if(this.getPriority() > o.getPriority()){
            return -1;
        }else{
            if(this.getExecutionTime() > o.getExecutionTime()){
                return 1;
            }else if(this.getExecutionTime() < o.getExecutionTime()){
                return -1;
            }else{
                return 0;
            }
        }
    }
    
}
