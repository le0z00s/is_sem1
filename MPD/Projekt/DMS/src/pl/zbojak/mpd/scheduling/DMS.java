/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zbojak.mpd.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import pl.zbojak.mpd.task.Task;

/**
 *
 * @author Rafał Zbojak
 */
public class DMS {

    private List<Task> tasks;
    private int SIMULATION_TIME;
    private List<Integer> simulationOrder;

    public DMS() {
        init();
    }

    public void init() {
        this.tasks = null;
        this.simulationOrder = null;
    }

    public void addTask(Task task) {
        if (this.tasks == null) {
            this.tasks = new ArrayList<Task>();
        }
        this.tasks.add(task);
    }

    public void addTasks(List<Task> tasks) {
        for (Task task : this.tasks) {
            this.tasks.add(task);
        }
    }

    public void simulate() {
        boolean taskAdded = false;
        int time = 1;
        
        while (time <= SIMULATION_TIME) {
            assignPriorities(time);
            Collections.sort(tasks);
            
            taskAdded = false;
            for(Task task : tasks){
                if(task.getPriority() >= 0){
                    task.addOccurancy();
                    if(task.getDuration() > 0){
                        time += task.getDuration();
                        for(int i = 0; i<task.getDuration(); i++){
                            getSimulationOrder().add(task.getTaskNumber());
                        }
                    }
                    
                    getSimulationOrder().add(task.getTaskNumber());
                    taskAdded = true;
                    break;
                }
            }
            if(!taskAdded){
                getSimulationOrder().add(-1);
            }
            time++;
        }
    }

    private void assignPriorities(int time) {
        int periodPart, executionTime, priority;
        for (Task task : tasks) {
            priority = (time - 1) / task.getPeriod() - task.getOccurrences();
            task.setPriority(priority);

            if (priority == 0) {
                periodPart = time % task.getPeriod();
                executionTime = task.getDeadline() - periodPart - task.getDuration();
                task.setExecutionTime(executionTime);
            } else {
                task.setExecutionTime(null);
            }
        }
    }

    public List<Task> getTasks() {
        if(this.tasks == null){
            this.tasks = new ArrayList<Task>();
        }
        return tasks;
    }

    public int getSIMULATION_TIME() {
        return SIMULATION_TIME;
    }
    
    public void setSIMULATION_TIME(int time){
        this.SIMULATION_TIME = time;
    }

    public List<Integer> getSimulationOrder() {
        if(this.simulationOrder == null){
            this.simulationOrder = new ArrayList<Integer>();
        }
        System.out.println(simulationOrder.toString());
        return simulationOrder;
    }
    
    
}
